package com.cognition.android.studentcompanion.fragments;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.cognition.android.studentcompanion.R;
import com.cognition.android.studentcompanion.pojo.UnitTimePeriod;
import com.cognition.android.studentcompanion.utils.UnitTimePeriodAdapter;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class WeekDayFragment extends Fragment {

    private List<UnitTimePeriod> unitTimePeriodList;
    private UnitTimePeriodAdapter unitTimePeriodAdapter;

    public WeekDayFragment() {
        // Required empty public constructor
    }

    @SuppressLint("ValidFragment")
    public WeekDayFragment(List<UnitTimePeriod> unitTimePeriodList) {
        this.unitTimePeriodList = unitTimePeriodList;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_week_day, container, false);

        final FrameLayout frameLayout = view.findViewById(R.id.lytWeekDayFrag);

        RecyclerView recyclerView = view.findViewById(R.id.lstUnitTimePeriods);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setHasFixedSize(true);

        unitTimePeriodAdapter = new UnitTimePeriodAdapter(getActivity().getApplicationContext(), unitTimePeriodList);
        recyclerView.setAdapter(unitTimePeriodAdapter);
        unitTimePeriodAdapter.notifyDataSetChanged();

        return view;
    }

    public UnitTimePeriodAdapter getUnitTimePeriodAdapter() {
        return this.unitTimePeriodAdapter;
    }
}
