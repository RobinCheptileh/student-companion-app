package com.cognition.android.studentcompanion.utils;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import com.cognition.android.studentcompanion.R;
import com.cognition.android.studentcompanion.activities.MainActivity;
import com.cognition.android.studentcompanion.activities.TimePeriodDetailActivity;
import com.cognition.android.studentcompanion.pojo.UnitTimePeriod;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.core.graphics.drawable.DrawableCompat;
import androidx.recyclerview.widget.RecyclerView;

/**
 * Unit time period adapter
 */

public class UnitTimePeriodAdapter extends RecyclerView.Adapter<UnitTimePeriodAdapter.UnitTimePeriodViewHolder> implements Filterable {

    private Context mContext;
    private Utils mUtils;
    private List<UnitTimePeriod> unitTimePeriodList;
    private List<UnitTimePeriod> unitTimePeriodListFiltered;

    class UnitTimePeriodViewHolder extends RecyclerView.ViewHolder {
        private LinearLayoutCompat lytItemParent;
        private AppCompatImageView imgOngoingIndicator;
        private AppCompatTextView txtUnitShortTime, txtUnitName, txtUnitVenue, txtUnitTime;

        UnitTimePeriodViewHolder(View view) {
            super(view);

            lytItemParent = view.findViewById(R.id.lytItemParent);
            imgOngoingIndicator = view.findViewById(R.id.imgOngoingIndicator);
            txtUnitShortTime = view.findViewById(R.id.txtUnitShortTime);
            txtUnitName = view.findViewById(R.id.txtUnitName);
            txtUnitVenue = view.findViewById(R.id.txtUnitVenue);
            txtUnitTime = view.findViewById(R.id.txtUnitTime);
        }
    }

    public UnitTimePeriodAdapter(Context context, List<UnitTimePeriod> unitTimePeriodList) {
        this.mContext = context;
        this.mUtils = new Utils(this.mContext);
        this.unitTimePeriodList = unitTimePeriodList;
        this.unitTimePeriodListFiltered = unitTimePeriodList;
    }

    @NonNull
    @Override
    public UnitTimePeriodViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.unit_time_period_item, parent, false);

        return new UnitTimePeriodViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull UnitTimePeriodViewHolder holder, int position) {
        // TODO: check unit times to enable status and verify

        Drawable unitVenueDrawable = DrawableCompat.wrap(mContext.getResources().getDrawable(R.drawable.ic_place_black_18dp));
        DrawableCompat.setTint(unitVenueDrawable, mContext.getResources().getColor(R.color.colorAccent));

        final UnitTimePeriod unitTimePeriod = this.unitTimePeriodListFiltered.get(position);

        holder.lytItemParent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, TimePeriodDetailActivity.class);
                intent.putExtra("unitTimePeriodId", unitTimePeriod.getId());
                intent.putExtra("unitName", unitTimePeriod.getUnit().getName());
                intent.putExtra("unitCode", unitTimePeriod.getUnit().getCode());
                intent.putExtra("unitVenue", unitTimePeriod.getVenue().getName());
                intent.putExtra("unitStartTime", unitTimePeriod.getTimePeriod().getStartTime());
                intent.putExtra("unitEndTime", unitTimePeriod.getTimePeriod().getEndTime());
                intent.putExtra("unitDay", unitTimePeriod.getDay().getName());
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                mContext.startActivity(intent);
            }
        });

        try {
            if (
                    new SimpleDateFormat("EEEEEEEEEE", Locale.ENGLISH)
                            .format(Calendar.getInstance().getTime()).equals(unitTimePeriod.getDay().getName())
                            && mUtils.checkIfCurrentTimeInRange(
                            unitTimePeriod.getTimePeriod().getStartTime(),
                            unitTimePeriod.getTimePeriod().getEndTime()
                    )
                    )
                holder.imgOngoingIndicator.setVisibility(View.VISIBLE);
            else
                holder.imgOngoingIndicator.setVisibility(View.GONE);
        } catch (ParseException e) {
            Log.w(MainActivity.TAG, e);
        }


        holder.txtUnitShortTime.setText(
                unitTimePeriod.getTimePeriod().getStartTime().substring(0, 1).equals("0") ? unitTimePeriod.getTimePeriod().getStartTime().substring(1, 2) : unitTimePeriod.getTimePeriod().getStartTime().substring(0, 2)
        );
        holder.txtUnitName.setText(String.format(
                "%s (%s)",
                unitTimePeriod.getUnit().getName(),
                unitTimePeriod.getUnit().getCode()
        ));
        holder.txtUnitVenue.setText(unitTimePeriod.getVenue().getName());
        holder.txtUnitVenue.setCompoundDrawablesWithIntrinsicBounds(unitVenueDrawable, null, null, null);
        holder.txtUnitTime.setText(
                unitTimePeriod.getTimePeriod().getStartTime().substring(0, 1).equals("0") ? unitTimePeriod.getTimePeriod().getStartTime().substring(1) : unitTimePeriod.getTimePeriod().getStartTime()
        );
    }

    @Override
    public int getItemCount() {
        return this.unitTimePeriodList.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                String searchText = constraint.toString().toLowerCase().trim();
                if (searchText.isEmpty())
                    unitTimePeriodListFiltered = unitTimePeriodList;
                else {
                    List<UnitTimePeriod> newFilteredList = new ArrayList<>();
                    for (UnitTimePeriod unitTimePeriod : unitTimePeriodList)
                        if (unitTimePeriod.getUnit().getName().toLowerCase().contains(searchText) ||
                                unitTimePeriod.getUnit().getCode().toLowerCase().contains(searchText) ||
                                unitTimePeriod.getVenue().getName().toLowerCase().contains(searchText) ||
                                unitTimePeriod.getTimePeriod().getStartTime().toLowerCase().contains(searchText))
                            newFilteredList.add(unitTimePeriod);

                    unitTimePeriodListFiltered = newFilteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = unitTimePeriodListFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                unitTimePeriodListFiltered = (ArrayList<UnitTimePeriod>) results.values;
                notifyDataSetChanged();
            }
        };
    }
}
