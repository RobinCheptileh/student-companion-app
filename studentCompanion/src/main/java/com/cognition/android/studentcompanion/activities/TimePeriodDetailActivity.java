package com.cognition.android.studentcompanion.activities;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.cognition.android.studentcompanion.R;
import com.cognition.android.studentcompanion.utils.Utils;

import java.text.ParseException;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.core.graphics.drawable.DrawableCompat;

public class TimePeriodDetailActivity extends AppCompatActivity {

    LinearLayoutCompat lytItemDetailParent;
    AppCompatTextView txtOngoingIndicator, txtUnitName, txtUnitVenue, txtUnitTime, txtUnitDay;
    AppCompatButton btnCancelUnitDetail, btnOkUnitDetail;

    Integer unitTimePeriodId;
    String unitName, unitCode, unitVenue, unitStartTime, unitEndTime, unitDay;
    boolean isOnGoing = false;

    Utils mUtils;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_time_period_detail);

        mUtils = new Utils(TimePeriodDetailActivity.this);

        initFields();
        initViews();
    }

    private void initFields() {
        unitTimePeriodId = getIntent().getIntExtra("unitTimePeriodId", 1);
        unitName = getIntent().getStringExtra("unitName");
        unitCode = getIntent().getStringExtra("unitCode");
        unitVenue = getIntent().getStringExtra("unitVenue");
        unitStartTime = getIntent().getStringExtra("unitStartTime");
        unitEndTime = getIntent().getStringExtra("unitEndTime");
        unitDay = getIntent().getStringExtra("unitDay");
    }

    private void initViews() {
        Drawable unitVenueDrawable = DrawableCompat.wrap(TimePeriodDetailActivity.this.getResources().getDrawable(R.drawable.ic_place_black_18dp));
        DrawableCompat.setTint(unitVenueDrawable, TimePeriodDetailActivity.this.getResources().getColor(R.color.colorAccent));

        Drawable unitTimeDrawable = DrawableCompat.wrap(TimePeriodDetailActivity.this.getResources().getDrawable(R.drawable.ic_access_time_black_18dp));
        DrawableCompat.setTint(unitTimeDrawable, TimePeriodDetailActivity.this.getResources().getColor(R.color.colorAccent));

        Drawable unitDayDrawable = DrawableCompat.wrap(TimePeriodDetailActivity.this.getResources().getDrawable(R.drawable.ic_event_black_24dp));
        DrawableCompat.setTint(unitDayDrawable, TimePeriodDetailActivity.this.getResources().getColor(R.color.colorAccent));

        lytItemDetailParent = findViewById(R.id.lytItemDetailParent);
        txtOngoingIndicator = findViewById(R.id.txtOngoingIndicator);
        txtUnitName = findViewById(R.id.txtUnitName);
        txtUnitVenue = findViewById(R.id.txtUnitVenue);
        txtUnitTime = findViewById(R.id.txtUnitTime);
        txtUnitDay = findViewById(R.id.txtUnitDay);
        btnCancelUnitDetail = findViewById(R.id.btnCancelUnitDetail);
        btnOkUnitDetail = findViewById(R.id.btnOkUnitDetail);

        txtUnitName.setText(String.format("%s (%s)", unitName, unitCode));
        txtUnitVenue.setText(unitVenue);
        txtUnitVenue.setCompoundDrawablesWithIntrinsicBounds(unitVenueDrawable, null, null, null);
        txtUnitTime.setText(String.format("%s - %s", unitStartTime, unitEndTime));
        txtUnitTime.setCompoundDrawablesWithIntrinsicBounds(unitTimeDrawable, null, null, null);
        txtUnitDay.setText(unitDay);
        txtUnitDay.setCompoundDrawablesWithIntrinsicBounds(unitDayDrawable, null, null, null);

        try {
            if (mUtils.checkIfCurrentTimeInRange(unitStartTime, unitEndTime)) {
                txtOngoingIndicator.setVisibility(View.VISIBLE);
                btnOkUnitDetail.setText(getString(R.string.verify));
                isOnGoing = true;
            } else {
                txtOngoingIndicator.setVisibility(View.GONE);
                isOnGoing = false;
            }
        } catch (ParseException e) {
            Log.w(MainActivity.TAG, e);
        }

        btnCancelUnitDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        btnOkUnitDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isOnGoing) {
                    Intent intent = new Intent(TimePeriodDetailActivity.this, VerifyActivity.class);
                    intent.putExtra("unitTimePeriodId", unitTimePeriodId);
                    startActivity(intent);
                    finish();
                } else
                    finish();
            }
        });
    }
}
