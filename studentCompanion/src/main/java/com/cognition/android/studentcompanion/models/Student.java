package com.cognition.android.studentcompanion.models;

import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.structure.BaseModel;

import java.util.Locale;

@Table(database = AppDatabase.class, allFields = true)
public class Student extends BaseModel {

    @PrimaryKey
    private Integer id;
    private String lastLogin;
    private String dateJoined;
    private Boolean isStaff;
    private String username;
    private String firstName;
    private String lastName;
    private String email;
    private String phone;
    private String dp;
    private String registrationNo;
    private Integer course;
    private Integer semester;
    private Integer year;
    private String password;

    /**
     * No args constructor for use in serialization
     */
    public Student() {
    }


    public Student(Integer id, String lastLogin, String dateJoined, Boolean isStaff, String username, String firstName, String lastName, String email, String phone, String dp, String registrationNo, Integer course, Integer semester, Integer year, String password) {
        super();
        this.id = id;
        this.lastLogin = lastLogin;
        this.dateJoined = dateJoined;
        this.isStaff = isStaff;
        this.username = username;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.phone = phone;
        this.dp = dp;
        this.registrationNo = registrationNo;
        this.course = course;
        this.semester = semester;
        this.year = year;
        this.password = password;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLastLogin() {
        return lastLogin;
    }

    public void setLastLogin(String lastLogin) {
        this.lastLogin = lastLogin;
    }

    public String getDateJoined() {
        return dateJoined;
    }

    public void setDateJoined(String dateJoined) {
        this.dateJoined = dateJoined;
    }

    public Boolean isStaff() {
        return isStaff;
    }

    public void setStaff(Boolean isStaff) {
        this.isStaff = isStaff;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getDp() {
        return dp;
    }

    public void setDp(String dp) {
        this.dp = dp;
    }

    public String getRegistrationNo() {
        return registrationNo;
    }

    public void setRegistrationNo(String registrationNo) {
        this.registrationNo = registrationNo;
    }

    public Integer getCourse() {
        return course;
    }

    public void setCourse(Integer course) {
        this.course = course;
    }

    public Integer getSemester() {
        return semester;
    }

    public void setSemester(Integer semester) {
        this.semester = semester;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public com.cognition.android.studentcompanion.pojo.Student getPojo() {
        return new com.cognition.android.studentcompanion.pojo.Student(
                this.id, this.lastLogin, this.dateJoined, this.isStaff, this.username,
                this.firstName, this.lastName, this.email, this.phone, this.dp, this.registrationNo,
                this.course, this.semester, this.year, this.password
        );
    }

    @Override
    public String toString() {
        return String.format(
                Locale.ENGLISH,
                "%d. %s %s - %s",
                this.id, this.firstName, this.lastName, this.registrationNo
        );
    }
}
