package com.cognition.android.studentcompanion.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Auth Token
 */

public class Token {

    @SerializedName("token")
    @Expose
    private String token;

    /**
     * No args constructor for use in serialization
     */
    public Token() {
    }

    /**
     * Constructor
     *
     * @param token String
     */
    public Token(String token) {
        this.token = token;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    @Override
    public String toString() {
        return String.format("Token: %s", this.token);
    }
}
