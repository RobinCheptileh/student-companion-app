package com.cognition.android.studentcompanion.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import com.google.android.material.textfield.TextInputLayout;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatEditText;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;

import com.cognition.android.studentcompanion.R;
import com.cognition.android.studentcompanion.utils.Utils;
import com.cognition.android.studentcompanion.utils.ApiClient;
import com.cognition.android.studentcompanion.utils.ApiInterface;
import com.cognition.android.studentcompanion.hotspotmanager.WifiApManager;
import com.cognition.android.studentcompanion.pojo.Student;
import com.cognition.android.studentcompanion.pojo.Token;
import androidx.transition.TransitionManager;
import com.wang.avi.AVLoadingIndicatorView;

import java.io.IOException;
import java.util.regex.Pattern;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {

    ConstraintLayout lytLoginParent;
    TextInputLayout txtUsernameWrap;
    AppCompatEditText txtUsername;
    TextInputLayout txtPasswordWrap;
    AppCompatEditText txtPassword;
    ViewGroup lytLoginButtonAnimContainer;
    AppCompatButton btnLogin;
    AVLoadingIndicatorView animLoginLoadIndicator;

    SharedPreferences sharedPref;
    Utils mUtils;
    WifiApManager wifiApManager;

    @Override
    protected void onStart() {
        super.onStart();

        sharedPref = LoginActivity.this.getSharedPreferences(getString(R.string.preferences_file_name), Context.MODE_PRIVATE);
        if (sharedPref.getBoolean(getString(R.string.is_logged_in_name), false)) {
            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.LoginTheme);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mUtils = new Utils(LoginActivity.this);
        wifiApManager = new WifiApManager(LoginActivity.this);

        initViews();
    }

    /**
     * Initialize views
     */
    private void initViews() {
        lytLoginParent = findViewById(R.id.lytLoginParent);
        txtUsernameWrap = findViewById(R.id.txtLoginUsernameWrap);
        txtUsername = findViewById(R.id.txtLoginUsername);
        txtPasswordWrap = findViewById(R.id.txtPasswordWrap);
        txtPassword = findViewById(R.id.txtPassword);
        lytLoginButtonAnimContainer = findViewById(R.id.lytLoginButtonAnimContainer);
        btnLogin = findViewById(R.id.btnLogin);
        animLoginLoadIndicator = findViewById(R.id.animLoginLoadIndicator);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String username = txtUsername.getText().toString().toUpperCase().trim().replaceAll(Pattern.quote("/"), "_");
                String password = txtPassword.getText().toString().trim();
                if (username.isEmpty())
                    txtUsernameWrap.setError(getString(R.string.field_is_empty));
                else if (password.isEmpty())
                    txtPasswordWrap.setError(getString(R.string.field_is_empty));
                else {
                    if (mUtils.getConnectivityStatus() != Utils.TYPE_NOT_CONNECTED) {
                        TransitionManager.beginDelayedTransition(lytLoginButtonAnimContainer);
                        btnLogin.setVisibility(View.GONE);
                        animLoginLoadIndicator.setVisibility(View.VISIBLE);

                        login(username, password);
                    } else
                        mUtils.showSnackbar(lytLoginParent, getString(R.string.an_error_occurred));
                }
                Log.d(MainActivity.TAG, username);
            }
        });
    }

    /**
     * Perform login call to server
     *
     * @param reg_no   String
     * @param password String
     */
    private void login(final String reg_no, String password) {
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<Token> call = apiInterface.getToken(reg_no, password);
        call.enqueue(new Callback<Token>() {
            @Override
            public void onResponse(@NonNull Call<Token> call, @NonNull Response<Token> response) {
                if (response.isSuccessful() && response.body() != null) {
                    SharedPreferences.Editor editor = sharedPref.edit();
                    editor.putString(
                            getString(R.string.auth_token_name),
                            String.format("Bearer %s", response.body().getToken())
                    );
                    editor.apply();
                    Log.d(MainActivity.TAG, String.format("Login success. Token %s", response.body().getToken()));

                    getStudentDetails(reg_no);
                } else {
                    try {
                        if (response.code() == 400) {
                            Log.d(MainActivity.TAG, response.errorBody().string());
                            TransitionManager.beginDelayedTransition(lytLoginButtonAnimContainer);
                            btnLogin.setVisibility(View.VISIBLE);
                            animLoginLoadIndicator.setVisibility(View.GONE);

                            txtUsernameWrap.setError(getString(R.string.invalid_reg_no_or_pass));
                        } else {
                            Log.d(MainActivity.TAG, response.errorBody().string());
                            TransitionManager.beginDelayedTransition(lytLoginButtonAnimContainer);
                            btnLogin.setVisibility(View.VISIBLE);
                            animLoginLoadIndicator.setVisibility(View.GONE);
                            mUtils.showSnackbar(lytLoginParent, getString(R.string.an_error_occurred));
                        }
                    } catch (IOException e) {
                        Log.e(MainActivity.TAG, e.toString());
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<Token> call, @NonNull Throwable t) {
                Log.e(MainActivity.TAG, t.toString());
                TransitionManager.beginDelayedTransition(lytLoginButtonAnimContainer);
                btnLogin.setVisibility(View.VISIBLE);
                animLoginLoadIndicator.setVisibility(View.GONE);
                mUtils.showSnackbar(lytLoginParent, getString(R.string.an_error_occurred));
            }
        });
    }

    /**
     * Gets student details
     *
     * @param username String
     */
    private void getStudentDetails(String username) {
        String authToken = sharedPref.getString(getString(R.string.auth_token_name), null);
        if (authToken != null) {
            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<Student> call = apiInterface.getStudentDetails(authToken, username);
            call.enqueue(new Callback<Student>() {
                @Override
                public void onResponse(@NonNull Call<Student> call, @NonNull Response<Student> response) {
                    if (response.isSuccessful() && response.body() != null) {
                        com.cognition.android.studentcompanion.models.Student student = response.body().getModel();
                        student.save();

                        SharedPreferences.Editor editor = sharedPref.edit();
                        editor.putInt(
                                getString(R.string.student_id_name),
                                student.getId()
                        );
                        editor.putString(
                                getString(R.string.student_username_name),
                                student.getUsername()
                        );
                        editor.putBoolean(
                                getString(R.string.is_logged_in_name),
                                true
                        );
                        editor.putBoolean(
                                getString(R.string.first_fetch_done_name),
                                false
                        );

                        editor.apply();

                        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        finish();
                    } else {
                        try {
                            if (response.code() == 400) {
                                Log.d(MainActivity.TAG, response.errorBody().string());
                                TransitionManager.beginDelayedTransition(lytLoginButtonAnimContainer);
                                btnLogin.setVisibility(View.VISIBLE);
                                animLoginLoadIndicator.setVisibility(View.GONE);

                                txtUsernameWrap.setError(getString(R.string.invalid_reg_no_or_pass));
                            } else {
                                Log.d(MainActivity.TAG, response.errorBody().string());
                                TransitionManager.beginDelayedTransition(lytLoginButtonAnimContainer);
                                btnLogin.setVisibility(View.VISIBLE);
                                animLoginLoadIndicator.setVisibility(View.GONE);
                                mUtils.showSnackbar(lytLoginParent, getString(R.string.an_error_occurred));
                            }
                        } catch (IOException e) {
                            Log.e(MainActivity.TAG, e.toString());
                        }
                    }
                }

                @Override
                public void onFailure(@NonNull Call<Student> call, @NonNull Throwable t) {
                    Log.e(MainActivity.TAG, t.toString());
                    TransitionManager.beginDelayedTransition(lytLoginButtonAnimContainer);
                    btnLogin.setVisibility(View.VISIBLE);
                    animLoginLoadIndicator.setVisibility(View.GONE);
                    mUtils.showSnackbar(lytLoginParent, getString(R.string.an_error_occurred));
                }
            });
        } else {
            Log.e(MainActivity.TAG, getString(R.string.missing_token_error));
            TransitionManager.beginDelayedTransition(lytLoginButtonAnimContainer);
            btnLogin.setVisibility(View.VISIBLE);
            animLoginLoadIndicator.setVisibility(View.GONE);
            mUtils.showSnackbar(lytLoginParent, getString(R.string.an_error_occurred));
        }
    }
}
