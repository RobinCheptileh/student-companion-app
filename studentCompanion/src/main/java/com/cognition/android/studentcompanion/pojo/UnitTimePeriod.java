
package com.cognition.android.studentcompanion.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UnitTimePeriod {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("unit")
    @Expose
    private Unit unit;
    @SerializedName("day")
    @Expose
    private Day day;
    @SerializedName("time_period")
    @Expose
    private TimePeriod timePeriod;
    @SerializedName("venue")
    @Expose
    private Venue venue;

    /**
     * No args constructor for use in serialization
     * 
     */
    public UnitTimePeriod() {
    }

    /**
     * 
     * @param id
     * @param unit
     * @param timePeriod
     * @param day
     * @param venue
     */
    public UnitTimePeriod(Integer id, Unit unit, Day day, TimePeriod timePeriod, Venue venue) {
        super();
        this.id = id;
        this.unit = unit;
        this.day = day;
        this.timePeriod = timePeriod;
        this.venue = venue;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Unit getUnit() {
        return unit;
    }

    public void setUnit(Unit unit) {
        this.unit = unit;
    }

    public Day getDay() {
        return day;
    }

    public void setDay(Day day) {
        this.day = day;
    }

    public TimePeriod getTimePeriod() {
        return timePeriod;
    }

    public void setTimePeriod(TimePeriod timePeriod) {
        this.timePeriod = timePeriod;
    }

    public Venue getVenue() {
        return venue;
    }

    public void setVenue(Venue venue) {
        this.venue = venue;
    }

}
