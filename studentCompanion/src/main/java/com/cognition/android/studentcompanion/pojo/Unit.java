
package com.cognition.android.studentcompanion.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Unit {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("course")
    @Expose
    private Course course;
    @SerializedName("semester")
    @Expose
    private Semester semester;
    @SerializedName("year")
    @Expose
    private Year year;

    /**
     * No args constructor for use in serialization
     * 
     */
    public Unit() {
    }

    /**
     * 
     * @param course
     * @param id
     * @param name
     * @param year
     * @param code
     * @param semester
     */
    public Unit(Integer id, String code, String name, Course course, Semester semester, Year year) {
        super();
        this.id = id;
        this.code = code;
        this.name = name;
        this.course = course;
        this.semester = semester;
        this.year = year;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    public Semester getSemester() {
        return semester;
    }

    public void setSemester(Semester semester) {
        this.semester = semester;
    }

    public Year getYear() {
        return year;
    }

    public void setYear(Year year) {
        this.year = year;
    }

}
