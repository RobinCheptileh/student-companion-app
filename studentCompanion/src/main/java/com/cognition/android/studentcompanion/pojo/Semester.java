
package com.cognition.android.studentcompanion.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Semester {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("semester")
    @Expose
    private Integer semester;

    /**
     * No args constructor for use in serialization
     * 
     */
    public Semester() {
    }

    /**
     * 
     * @param id
     * @param semester
     */
    public Semester(Integer id, Integer semester) {
        super();
        this.id = id;
        this.semester = semester;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getSemester() {
        return semester;
    }

    public void setSemester(Integer semester) {
        this.semester = semester;
    }

}
