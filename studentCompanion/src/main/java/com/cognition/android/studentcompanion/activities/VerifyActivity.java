package com.cognition.android.studentcompanion.activities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.cognition.android.studentcompanion.R;
import com.cognition.android.studentcompanion.pojo.ConfidenceMessage;
import com.cognition.android.studentcompanion.utils.ApiClient;
import com.cognition.android.studentcompanion.utils.ApiInterface;
import com.cognition.android.studentcompanion.utils.Utils;
import com.wonderkiln.camerakit.CameraKit;
import com.wonderkiln.camerakit.CameraKitEventCallback;
import com.wonderkiln.camerakit.CameraKitImage;
import com.wonderkiln.camerakit.CameraView;

import java.io.File;
import java.io.IOException;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.transition.TransitionManager;
import id.zelory.compressor.Compressor;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class VerifyActivity extends AppCompatActivity {

    ConstraintLayout lytParent, lytControls;
    CameraView camera;
    AppCompatImageView changeCam, closeCam, takePic, showInfo;
    View lytCapture;
    LinearLayoutCompat lytVerifyingLoading;

    SharedPreferences sharedPref;
    Utils mUtils;
    Integer unitTimePeriodId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verify);

        sharedPref = VerifyActivity.this.getSharedPreferences(getString(R.string.preferences_file_name), Context.MODE_PRIVATE);
        mUtils = new Utils(VerifyActivity.this);

        unitTimePeriodId = getIntent().getIntExtra("unitTimePeriodId", 1);

        initViews();
    }

    /**
     * Method called when activity is paused
     */
    @Override
    protected void onPause() {
        super.onPause();
        camera.stop();
    }

    /**
     * Method called when activity is resumed
     */
    @Override
    protected void onResume() {
        super.onResume();
        camera.start();
    }

    /**
     * Method called when activity is stopped
     */
    @Override
    protected void onStop() {
        super.onStop();
    }

    /**
     * Method called when activity is destroyed
     */
    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    /**
     * Method called when back key is pressed
     */
    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    /**
     * Initialize views
     */
    private void initViews() {
        lytParent = findViewById(R.id.lytParent);
        lytControls = findViewById(R.id.lytControls);
        lytCapture = findViewById(R.id.lytCapture);
        camera = findViewById(R.id.camera);
        changeCam = findViewById(R.id.changeCam);
        closeCam = findViewById(R.id.closeCam);
        takePic = findViewById(R.id.takePic);
        showInfo = findViewById(R.id.showInfo);
        lytVerifyingLoading = findViewById(R.id.lytVerifyingLoading);

        camera.setFacing(CameraKit.Constants.FACING_FRONT);
        camera.start();

        changeCam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                camera.setFacing(
                        camera.getFacing() == CameraKit.Constants.FACING_FRONT ? CameraKit.Constants.FACING_BACK : CameraKit.Constants.FACING_FRONT
                );
            }
        });

        closeCam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        takePic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mUtils.getConnectivityStatus() == Utils.TYPE_WIFI) {
                    TransitionManager.beginDelayedTransition(lytParent);
                    lytControls.setVisibility(View.GONE);
                    lytCapture.setVisibility(View.VISIBLE);
                    lytCapture.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            TransitionManager.beginDelayedTransition(lytParent);
                            lytCapture.setVisibility(View.GONE);
                        }
                    }, 200);
                    camera.captureImage(new CameraKitEventCallback<CameraKitImage>() {
                        @Override
                        public void callback(CameraKitImage cameraKitImage) {
                            try {
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        TransitionManager.beginDelayedTransition(lytParent);
                                        lytVerifyingLoading.setVisibility(View.VISIBLE);
                                        camera.stop();
                                    }
                                });

                                File image = new Compressor(VerifyActivity.this)
                                        .setQuality(30)
                                        .setCompressFormat(Bitmap.CompressFormat.JPEG)
                                        .compressToFile(mUtils.bitmapToFile(cameraKitImage.getBitmap()));

                                RequestBody requestFile = RequestBody.create(MediaType.parse("image/jpeg"), image);
                                MultipartBody.Part requestBody = MultipartBody.Part.createFormData("image", image.getName(), requestFile);

                                ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
                                Call<ConfidenceMessage> call = apiInterface.verifyAttendance(
                                        sharedPref.getString(getString(R.string.auth_token_name), ""),
                                        sharedPref.getString(VerifyActivity.this.getString(R.string.student_username_name), ""),
                                        RequestBody.create(MediaType.parse("text/plain"), mUtils.getIPAddress(true)),
                                        RequestBody.create(MediaType.parse("text/plain"), mUtils.getSSID().replace("\"", "")),
                                        RequestBody.create(MediaType.parse("text/plain"), String.valueOf(unitTimePeriodId)),
                                        requestBody
                                );
                                call.enqueue(new Callback<ConfidenceMessage>() {
                                    @Override
                                    public void onResponse(@NonNull Call<ConfidenceMessage> call, @NonNull final Response<ConfidenceMessage> response) {
                                        if (response.isSuccessful()) {
                                            runOnUiThread(new Runnable() {
                                                @Override
                                                public void run() {
                                                    TransitionManager.beginDelayedTransition(lytParent);
                                                    lytVerifyingLoading.setVisibility(View.GONE);
                                                    lytControls.setVisibility(View.VISIBLE);
                                                    camera.start();

                                                    showDialog(getString(R.string.success), getString(R.string.verification_success), new DialogInterface.OnClickListener() {
                                                        @Override
                                                        public void onClick(DialogInterface dialog, int which) {
                                                            dialog.dismiss();
                                                            finish();
                                                        }
                                                    }, new DialogInterface.OnCancelListener() {
                                                        @Override
                                                        public void onCancel(DialogInterface dialog) {
                                                            finish();
                                                        }
                                                    });
                                                }
                                            });
                                        } else {
                                            runOnUiThread(new Runnable() {
                                                @Override
                                                public void run() {
                                                    try {
                                                        TransitionManager.beginDelayedTransition(lytParent);
                                                        lytVerifyingLoading.setVisibility(View.GONE);
                                                        lytControls.setVisibility(View.VISIBLE);
                                                        camera.start();

                                                        Log.e(MainActivity.TAG, response.errorBody().string());
                                                        mUtils.showSnackbar(lytParent, getString(R.string.verification_failed));
                                                    } catch (IOException e) {
                                                        TransitionManager.beginDelayedTransition(lytParent);
                                                        lytVerifyingLoading.setVisibility(View.GONE);
                                                        lytControls.setVisibility(View.VISIBLE);
                                                        camera.start();

                                                        Log.w(MainActivity.TAG, e);
                                                    }
                                                }
                                            });
                                        }
                                    }

                                    @Override
                                    public void onFailure(@NonNull Call<ConfidenceMessage> call, @NonNull final Throwable t) {
                                        runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                TransitionManager.beginDelayedTransition(lytParent);
                                                lytVerifyingLoading.setVisibility(View.GONE);
                                                lytControls.setVisibility(View.VISIBLE);
                                                camera.start();

                                                Log.w(MainActivity.TAG, t);
                                                mUtils.showSnackbar(lytParent, getString(R.string.verification_failed));
                                            }
                                        });
                                    }
                                });
                            } catch (final IOException e) {
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        TransitionManager.beginDelayedTransition(lytParent);
                                        lytVerifyingLoading.setVisibility(View.GONE);
                                        lytControls.setVisibility(View.VISIBLE);
                                        camera.start();

                                        Log.w(MainActivity.TAG, e);
                                    }
                                });
                            }
                        }
                    });
                } else {
                    mUtils.showSnackbar(lytParent, getString(R.string.please_turn_on_wifi));
                }
            }
        });

        showInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialog(getString(R.string.info), getString(R.string.verification_info_text), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }, new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialog) {
                        dialog.dismiss();
                    }
                });
            }
        });
    }

    /**
     * Show an AlertDialog
     *
     * @param title            String
     * @param message          String
     * @param onCancelListener DialogInterface.OnCancelListener
     */
    private void showDialog(String title, String message, DialogInterface.OnClickListener onClickListener, DialogInterface.OnCancelListener onCancelListener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(VerifyActivity.this);
        builder.setIcon(getResources().getDrawable(R.drawable.ic_launcher_high_res));
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton("Okay", onClickListener);
        builder.setOnCancelListener(onCancelListener);

        AlertDialog dialog = builder.create();
        dialog.show();
    }
}
