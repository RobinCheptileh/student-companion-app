package com.cognition.android.studentcompanion.models;

import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.structure.BaseModel;

/**
 * UnitTimePeriod call response string
 */

@Table(database = AppDatabase.class, allFields = true)
public class UnitTimePeriodString extends BaseModel {

    @PrimaryKey
    private Integer id;
    private String unitTimePeriodResponseString;

    public UnitTimePeriodString() {
    }

    public UnitTimePeriodString(Integer id, String unitTimePeriodResponseString) {
        this.id = id;
        this.unitTimePeriodResponseString = unitTimePeriodResponseString;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUnitTimePeriodResponseString() {
        return unitTimePeriodResponseString;
    }

    public void setUnitTimePeriodResponseString(String unitTimePeriodResponseString) {
        this.unitTimePeriodResponseString = unitTimePeriodResponseString;
    }
}
