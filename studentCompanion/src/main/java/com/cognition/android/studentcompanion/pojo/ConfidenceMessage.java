package com.cognition.android.studentcompanion.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ConfidenceMessage {

    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("confidence")
    @Expose
    private String confidence;

    public ConfidenceMessage() {
    }

    public ConfidenceMessage(String message, String confidence) {
        this.message = message;
        this.confidence = confidence;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getConfidence() {
        return confidence;
    }

    public void setConfidence(String confidence) {
        this.confidence = confidence;
    }
}
