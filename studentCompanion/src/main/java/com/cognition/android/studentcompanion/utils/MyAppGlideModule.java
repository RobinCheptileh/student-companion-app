package com.cognition.android.studentcompanion.utils;

import com.bumptech.glide.annotation.GlideModule;
import com.bumptech.glide.module.AppGlideModule;

/**
 * Glide Module
 */

@GlideModule
public final class MyAppGlideModule extends AppGlideModule {
}
