package com.cognition.android.studentcompanion.utils;

import com.cognition.android.studentcompanion.pojo.ConfidenceMessage;
import com.cognition.android.studentcompanion.pojo.Student;
import com.cognition.android.studentcompanion.pojo.Token;
import com.cognition.android.studentcompanion.pojo.UnitTimePeriod;

import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;

/**
 * API Interface
 */

public interface ApiInterface {
    @FormUrlEncoded
    @POST("api-token-auth/")
    Call<Token> getToken(
            @Field("username") String username,
            @Field("password") String password
    );

    @GET("students/{username}/")
    Call<Student> getStudentDetails(
            @Header("Authorization") String token,
            @Path("username") String username
    );

    @GET("unit-time-periods/{username}/")
    Call<List<UnitTimePeriod>> getUnitTimePeriods(
            @Header("Authorization") String token,
            @Path("username") String username
    );

    @Multipart
    @POST("students/verify/{username}/")
    Call<ConfidenceMessage> verifyAttendance(
            @Header("Authorization") String token,
            @Path("username") String username,
            @Part("ip") RequestBody ip,
            @Part("lec_hall") RequestBody ssid,
            @Part("unit_time_period_id") RequestBody unitTimePeriodId,
            @Part MultipartBody.Part file
    );
}
