package com.cognition.android.studentcompanion.utils;

import com.cognition.android.studentcompanion.R;
import com.cognition.android.studentcompanion.StudentCompanionApplication;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Returns retrofit instance
 */

public class ApiClient {
    public static final String BASE_URL = StudentCompanionApplication.getAppResources().getString(R.string.api_address);
    private static Retrofit retrofit = null;

    public static Retrofit getClient() {
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(new OkHttpClient.Builder()
                            .readTimeout(3, TimeUnit.MINUTES)
                            .connectTimeout(3, TimeUnit.MINUTES)
                            .build())
                    .build();
        }
        return retrofit;
    }
}
