package com.cognition.android.studentcompanion.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;

import com.cognition.android.studentcompanion.BuildConfig;
import com.cognition.android.studentcompanion.R;
import com.cognition.android.studentcompanion.fragments.WeekDayFragment;
import com.cognition.android.studentcompanion.models.AppDatabase;
import com.cognition.android.studentcompanion.models.Student;
import com.cognition.android.studentcompanion.models.Student_Table;
import com.cognition.android.studentcompanion.models.UnitTimePeriodString;
import com.cognition.android.studentcompanion.models.UnitTimePeriodString_Table;
import com.cognition.android.studentcompanion.pojo.UnitTimePeriod;
import com.cognition.android.studentcompanion.utils.ApiClient;
import com.cognition.android.studentcompanion.utils.ApiInterface;
import com.cognition.android.studentcompanion.utils.GlideApp;
import com.cognition.android.studentcompanion.utils.Utils;
import com.cognition.android.studentcompanion.utils.ViewPagerAdapter;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.tabs.TabLayout;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.raizlabs.android.dbflow.config.FlowManager;
import com.raizlabs.android.dbflow.sql.language.SQLite;
import com.transitionseverywhere.ChangeText;
import com.wang.avi.AVLoadingIndicatorView;

import java.io.IOException;
import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.transition.TransitionManager;
import androidx.viewpager.widget.ViewPager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    public static final String TAG = "studentCompanion";

    Utils mUtils;

    private FirebaseAnalytics mFirebaseAnalytics;

    private SharedPreferences sharedPref;

    private Student student;

    Gson mGson;

    CoordinatorLayout contentFrame;
    CollapsingToolbarLayout collapsingToolbarLayout;
    AppBarLayout appBarLayout;
    Toolbar toolbar;
    DrawerLayout mDrawerLayout;
    AppCompatTextView txtDay;
    AppCompatTextView txtMonthYear;
    TabLayout weekDayTabs;
    ViewPager weekDayViewPager;
    ConstraintLayout lytLoading;
    AVLoadingIndicatorView animLoginLoadIndicator;
    AppCompatImageView imgErrorImage;
    AppCompatTextView txtFetchStatus;

    ViewPagerAdapter viewPagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        sharedPref = MainActivity.this.getSharedPreferences(getString(R.string.preferences_file_name), Context.MODE_PRIVATE);
        mUtils = new Utils(MainActivity.this);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(MainActivity.this);
        student = SQLite.select().from(Student.class)
                .where(Student_Table.id.eq(sharedPref.getInt(getString(R.string.student_id_name), 0)))
                .querySingle();
        mGson = new Gson();

        initViews();
    }

    /**
     * Method called when activity is paused
     */
    @Override
    protected void onPause() {
        super.onPause();
    }

    /**
     * Method called when activity is resumed
     */
    @Override
    protected void onResume() {
        super.onResume();
    }

    /**
     * Method called when activity is stopped
     */
    @Override
    protected void onStop() {
        super.onStop();
    }

    /**
     * Method called when activity is destroyed
     */
    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    /**
     * Method called when back key is pressed
     */
    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    /**
     * Log out the user
     */
    private void logOut() {
        sharedPref.edit().clear().apply();
        FlowManager.getDatabase(AppDatabase.NAME).reset();

        Intent intent = new Intent(MainActivity.this, LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }

    /**
     * Initialize views
     */
    private void initViews() {
        contentFrame = findViewById(R.id.content_frame);
        collapsingToolbarLayout = findViewById(R.id.collapsing_toolbar);
        appBarLayout = findViewById(R.id.app_bar_layout);
        toolbar = findViewById(R.id.toolbar);
        mDrawerLayout = findViewById(R.id.drawer_layout);
        txtDay = findViewById(R.id.txtDay);
        txtMonthYear = findViewById(R.id.txtMonthYear);
        weekDayTabs = findViewById(R.id.week_day_tabs);
        weekDayViewPager = findViewById(R.id.week_day_viewpager);
        lytLoading = findViewById(R.id.lytLoading);
        animLoginLoadIndicator = findViewById(R.id.animLoginLoadIndicator);
        imgErrorImage = findViewById(R.id.imgErrorImage);
        txtFetchStatus = findViewById(R.id.txtFetchStatus);

        toolbar = findViewById(R.id.toolbar);
        toolbar.inflateMenu(R.menu.main_menu);
        toolbar.setTitle(R.string.empty_text);
        toolbar.setNavigationIcon(R.drawable.ic_menu);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDrawerLayout.openDrawer(GravityCompat.START);
            }
        });
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.refresh:
                        setupViewPager(false);
                        break;
                }

                return true;
            }
        });

        collapsingToolbarLayout.setExpandedTitleColor(getResources().getColor(android.R.color.transparent));
        collapsingToolbarLayout.setContentScrimColor(getResources().getColor(R.color.colorPrimary));
        collapsingToolbarLayout.setStatusBarScrimColor(getResources().getColor(R.color.colorPrimaryDark));

        mDrawerLayout.addDrawerListener(
                new DrawerLayout.DrawerListener() {
                    @Override
                    public void onDrawerSlide(@NonNull View drawerView, float slideOffset) {
                        // Respond when the drawer's position changes
                    }

                    @Override
                    public void onDrawerOpened(@NonNull View drawerView) {
                        // Respond when the drawer is opened
                    }

                    @Override
                    public void onDrawerClosed(@NonNull View drawerView) {
                        // Respond when the drawer is closed
                    }

                    @Override
                    public void onDrawerStateChanged(int newState) {
                        // Respond when the drawer motion state changes
                    }
                }
        );

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                        // set item as selected to persist highlight
                        // menuItem.setChecked(true);
                        // close drawer when item is tapped
                        mDrawerLayout.closeDrawers();

                        switch (menuItem.getItemId()) {
                            case R.id.logout:
                                logOut();
                                break;

                            case R.id.about:
                                mUtils.showSnackbar(contentFrame, String.format(
                                        "%s version %s",
                                        getResources().getString(R.string.app_name),
                                        BuildConfig.VERSION_NAME
                                ));
                                break;
                        }

                        return true;
                    }
                });

        AppCompatImageView imgProfilePic = navigationView.getHeaderView(0).findViewById(R.id.imgProfilePic);
        AppCompatTextView txtProfileName = navigationView.getHeaderView(0).findViewById(R.id.txtProfileName);
        AppCompatTextView txtProfileRegNo = navigationView.getHeaderView(0).findViewById(R.id.txtProfileRegNo);

        GlideApp.with(MainActivity.this)
                .load(String.format("%s%s", getString(R.string.base_url), student.getDp()))
                .circleCrop()
                .placeholder(R.drawable.ic_launcher_high_res)
                .into(imgProfilePic);
        txtProfileName.setText(String.format("%s %s", student.getFirstName(), student.getLastName()));
        txtProfileRegNo.setText(student.getRegistrationNo());

        txtDay.setText(new SimpleDateFormat("dd", Locale.ENGLISH).format(Calendar.getInstance().getTime()));
        txtMonthYear.setText(
                new SimpleDateFormat("EEE MMM, yyyy", Locale.ENGLISH)
                        .format(Calendar.getInstance().getTime())
        );

        weekDayTabs.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                weekDayViewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        weekDayTabs.setupWithViewPager(weekDayViewPager);
        setupViewPager(true);
    }

    /**
     * Set up the view pager
     *
     * @param useCache boolean
     */
    private void setupViewPager(boolean useCache) {
        TransitionManager.beginDelayedTransition(contentFrame);
        weekDayViewPager.setVisibility(View.GONE);
        lytLoading.setVisibility(View.VISIBLE);
        animLoginLoadIndicator.smoothToShow();
        imgErrorImage.setVisibility(View.GONE);
        txtFetchStatus.setText(getString(R.string.fetching_units));

        if (useCache) {
            boolean firstFetchDone = sharedPref.getBoolean(getString(R.string.first_fetch_done_name), false);
            if (firstFetchDone) {
                UnitTimePeriodString unitTimePeriodString = SQLite.select()
                        .from(UnitTimePeriodString.class)
                        .where(UnitTimePeriodString_Table.id.eq(0))
                        .querySingle();

                Type unitTimePeriodListType = new TypeToken<Collection<UnitTimePeriod>>() {
                }.getType();
                List<UnitTimePeriod> unitTimePeriodList = mGson.fromJson(unitTimePeriodString.getUnitTimePeriodResponseString(), unitTimePeriodListType);

                updateViewPager(splitToWeekDays(unitTimePeriodList));
            } else
                fetchUnitTimePeriods();
        } else
            fetchUnitTimePeriods();
    }

    /**
     * Fetch unit time periods from server
     */
    private void fetchUnitTimePeriods() {
        String authToken = sharedPref.getString(getString(R.string.auth_token_name), null);
        String studentUsername = sharedPref.getString(getString(R.string.student_username_name), null);
        if (authToken != null && studentUsername != null) {
            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<List<UnitTimePeriod>> call = apiInterface.getUnitTimePeriods(authToken, studentUsername);
            call.enqueue(new Callback<List<UnitTimePeriod>>() {
                @Override
                public void onResponse(@NonNull Call<List<UnitTimePeriod>> call, @NonNull Response<List<UnitTimePeriod>> response) {
                    try {
                        if (response.isSuccessful() && response.body() != null) {
                            UnitTimePeriodString unitTimePeriodString = new UnitTimePeriodString(0, mGson.toJson(response.body()));
                            unitTimePeriodString.update();

                            SharedPreferences.Editor editor = sharedPref.edit();
                            editor.putBoolean(getString(R.string.first_fetch_done_name), true);
                            editor.apply();

                            updateViewPager(splitToWeekDays(response.body()));
                        } else {
                            Log.e(MainActivity.TAG, response.errorBody().string());
                            TransitionManager.beginDelayedTransition(contentFrame);
                            weekDayViewPager.setVisibility(View.GONE);
                            lytLoading.setVisibility(View.VISIBLE);
                            animLoginLoadIndicator.smoothToHide();
                            imgErrorImage.setVisibility(View.VISIBLE);
                            TransitionManager.beginDelayedTransition(contentFrame,
                                    new ChangeText().setChangeBehavior(ChangeText.CHANGE_BEHAVIOR_OUT_IN));
                            txtFetchStatus.setText(getString(R.string.an_error_occurred));
                        }
                    } catch (IOException e) {
                        Log.w(MainActivity.TAG, e);
                        TransitionManager.beginDelayedTransition(contentFrame);
                        weekDayViewPager.setVisibility(View.GONE);
                        lytLoading.setVisibility(View.VISIBLE);
                        animLoginLoadIndicator.smoothToHide();
                        imgErrorImage.setVisibility(View.VISIBLE);
                        TransitionManager.beginDelayedTransition(contentFrame,
                                new ChangeText().setChangeBehavior(ChangeText.CHANGE_BEHAVIOR_OUT_IN));
                        txtFetchStatus.setText(getString(R.string.an_error_occurred));
                    }
                }

                @Override
                public void onFailure(@NonNull Call<List<UnitTimePeriod>> call, @NonNull Throwable t) {
                    Log.w(MainActivity.TAG, t);
                    TransitionManager.beginDelayedTransition(contentFrame);
                    weekDayViewPager.setVisibility(View.GONE);
                    lytLoading.setVisibility(View.VISIBLE);
                    animLoginLoadIndicator.smoothToHide();
                    imgErrorImage.setVisibility(View.VISIBLE);
                    TransitionManager.beginDelayedTransition(contentFrame,
                            new ChangeText().setChangeBehavior(ChangeText.CHANGE_BEHAVIOR_OUT_IN));
                    txtFetchStatus.setText(getString(R.string.an_error_occurred));
                }
            });
        }
    }

    /**
     * Add fragements to view pager adapter and add adapter to view pager
     *
     * @param unitTimePeriodDays Map
     */
    private void updateViewPager(Map<String, List<UnitTimePeriod>> unitTimePeriodDays) {
        viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());

        if (unitTimePeriodDays.containsKey("Monday"))
            viewPagerAdapter.addFrag(new WeekDayFragment(unitTimePeriodDays.get("Monday")), "Mon");

        if (unitTimePeriodDays.containsKey("Tuesday"))
            viewPagerAdapter.addFrag(new WeekDayFragment(unitTimePeriodDays.get("Tuesday")), "Tue");

        if (unitTimePeriodDays.containsKey("Wednesday"))
            viewPagerAdapter.addFrag(new WeekDayFragment(unitTimePeriodDays.get("Wednesday")), "Wed");

        if (unitTimePeriodDays.containsKey("Thursday"))
            viewPagerAdapter.addFrag(new WeekDayFragment(unitTimePeriodDays.get("Thursday")), "Thu");

        if (unitTimePeriodDays.containsKey("Friday"))
            viewPagerAdapter.addFrag(new WeekDayFragment(unitTimePeriodDays.get("Friday")), "Fri");

        weekDayViewPager.setAdapter(viewPagerAdapter);
        viewPagerAdapter.notifyDataSetChanged();
        weekDayTabs.setupWithViewPager(weekDayViewPager);

        TransitionManager.beginDelayedTransition(contentFrame);
        weekDayViewPager.setVisibility(View.VISIBLE);
        animLoginLoadIndicator.smoothToHide();
        lytLoading.setVisibility(View.GONE);
        TransitionManager.endTransitions(contentFrame);

        int today = viewPagerAdapter.getItemPositionByTitle(
                new SimpleDateFormat("EEE", Locale.ENGLISH).format(
                        Calendar.getInstance().getTime()
                )
        );
        Log.d(MainActivity.TAG, String.format("Today: %d", today));
        weekDayViewPager.setCurrentItem(today != -1 ? today : 0);
        viewPagerAdapter.notifyDataSetChanged();
    }

    /**
     * Group unit time periods by day
     *
     * @param unitTimePeriodList List
     * @return Map
     */
    private Map<String, List<UnitTimePeriod>> splitToWeekDays(List<UnitTimePeriod> unitTimePeriodList) {
        Map<String, List<UnitTimePeriod>> unitTimePeriodDays = new HashMap<>();
        for (UnitTimePeriod unitTimePeriod : unitTimePeriodList) {
            String day = unitTimePeriod.getDay().getName();
            if (unitTimePeriodDays.containsKey(day))
                unitTimePeriodDays.get(day).add(unitTimePeriod);
            else {
                List<UnitTimePeriod> dayList = new ArrayList<>();
                dayList.add(unitTimePeriod);
                unitTimePeriodDays.put(day, dayList);
            }
        }

        return unitTimePeriodDays;
    }
}
