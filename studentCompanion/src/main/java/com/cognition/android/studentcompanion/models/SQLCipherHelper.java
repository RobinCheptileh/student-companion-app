package com.cognition.android.studentcompanion.models;

import com.cognition.android.studentcompanion.R;
import com.cognition.android.studentcompanion.StudentCompanionApplication;
import com.raizlabs.android.dbflow.config.DatabaseDefinition;
import com.raizlabs.android.dbflow.sqlcipher.SQLCipherOpenHelper;
import com.raizlabs.android.dbflow.structure.database.DatabaseHelperListener;

/**
 * SQLCipher Helper
 */

public class SQLCipherHelper extends SQLCipherOpenHelper {
    public SQLCipherHelper(DatabaseDefinition databaseDefinition, DatabaseHelperListener listener) {
        super(databaseDefinition, listener);
    }

    @Override
    protected String getCipherSecret() {
        return StudentCompanionApplication.getAppResources().getString(R.string.hex_256);
    }
}
