package com.cognition.android.studentcompanionserver.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiConfiguration;
import androidx.annotation.NonNull;
import com.google.android.material.snackbar.Snackbar;
import android.util.Log;
import android.view.View;

import com.cognition.android.studentcompanionserver.activities.MainActivity;
import com.cognition.android.studentcompanionserver.R;
import com.cognition.android.studentcompanionserver.hotspotmanager.ClientScanResult;
import com.cognition.android.studentcompanionserver.pojo.ServerDevice;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.FirebaseFirestore;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * General utilities
 */

public class Utils {

    public static final int TYPE_WIFI = 1;
    public static final int TYPE_MOBILE = 2;
    public static final int TYPE_NOT_CONNECTED = 0;

    /**
     * Context
     */
    private Context mContext;
    private SharedPreferences sharedPref;
    private FirebaseFirestore db;

    public Utils(Context context) {
        this.mContext = context;
        this.sharedPref = mContext.getSharedPreferences(mContext.getString(R.string.preferences_file_name), Context.MODE_PRIVATE);
        this.db = FirebaseFirestore.getInstance();
    }

    /**
     * Update list of connected device to Firestore
     *
     * @param connectedDevices List
     */
    public void updateConnectedDevicesToFS(List<ClientScanResult> connectedDevices) {
        final int deviceID = sharedPref.getInt(mContext.getString(R.string.device_id_name), 0);

        if (deviceID != 0) {
            Map<String, ClientScanResult> deviceMap = new HashMap<>();
            for (ClientScanResult clientScanResult : connectedDevices)
                deviceMap.put(clientScanResult.getIpAddr(), clientScanResult);

            setAddDeviceToFirestoreSuccess(false);
            db.collection("serverDevices").document(String.valueOf(deviceID)).set(deviceMap).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    if (task.isSuccessful()) {
                        Log.d(MainActivity.TAG, String.format("List for device %d updated successfully", deviceID));
                        setAddDeviceToFirestoreSuccess(true);
                    } else {
                        Log.w(MainActivity.TAG, task.getException());
                    }
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Log.w(MainActivity.TAG, e);
                }
            });
        } else
            Log.e(MainActivity.TAG, mContext.getString(R.string.device_id_not_init));
    }

    /**
     * Update Firebase Token on server
     *
     * @param newToken String
     */
    public void updateFirebaseToken(String newToken) {
        String authToken = this.sharedPref.getString(mContext.getString(R.string.auth_token_name), null);
        int deviceID = sharedPref.getInt(mContext.getString(R.string.device_id_name), 0);
        if (authToken != null && deviceID != 0) {
            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<ServerDevice> call = apiInterface.updateServerToken(authToken, deviceID, newToken);
            call.enqueue(new Callback<ServerDevice>() {
                @Override
                public void onResponse(@NonNull Call<ServerDevice> call, @NonNull Response<ServerDevice> response) {
                    if (response.isSuccessful()) {
                        Log.d(MainActivity.TAG, "Token update successful");
                        setFATokenUpdateSuccess(true);
                    } else {
                        Log.e(MainActivity.TAG, "Token update failed");
                        setFATokenUpdateSuccess(false);
                    }
                }

                @Override
                public void onFailure(@NonNull Call<ServerDevice> call, @NonNull Throwable t) {
                    Log.e(MainActivity.TAG, String.format("Token update failed: %s", t.toString()));
                    setFATokenUpdateSuccess(false);
                }
            });
        } else {
            Log.e(MainActivity.TAG, mContext.getString(R.string.missing_token_error));
            setFATokenUpdateSuccess(false);
        }
    }

    /**
     * Set if device was added to Firestore successfully
     *
     * @param status boolean
     */
    public void setAddDeviceToFirestoreSuccess(boolean status) {
        SharedPreferences.Editor editor = this.sharedPref.edit();
        editor.putBoolean(mContext.getString(R.string.add_device_to_fs_name), status);
        editor.apply();
    }

    /**
     * Set if Firebase token update was successful
     *
     * @param status boolean
     */
    public void setFATokenUpdateSuccess(boolean status) {
        SharedPreferences.Editor editor = this.sharedPref.edit();
        editor.putBoolean(mContext.getString(R.string.fa_token_update_success_name), status);
        editor.apply();
    }

    /**
     * Set wifi config ssid and password with common settings
     *
     * @param SSID     String
     * @param Password String
     * @return WifiConfiguration
     */
    public WifiConfiguration setWifiConfig(@NonNull String SSID, @NonNull String Password, boolean isHidden) {
        WifiConfiguration wifiConfiguration = new WifiConfiguration();
        wifiConfiguration.SSID = String.format("\"%s\"", SSID);
        wifiConfiguration.preSharedKey = String.format("\"%s\"", Password);
        wifiConfiguration.hiddenSSID = isHidden;
        wifiConfiguration.status = WifiConfiguration.Status.ENABLED;
        wifiConfiguration.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.TKIP);
        wifiConfiguration.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.CCMP);
        wifiConfiguration.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.WPA_PSK);
        wifiConfiguration.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.TKIP);
        wifiConfiguration.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.CCMP);
        wifiConfiguration.allowedProtocols.set(WifiConfiguration.Protocol.RSN);

        return wifiConfiguration;
    }

    /**
     * Display messages on the snackbar
     *
     * @param message String
     */
    public void showSnackbar(View view, String message) {
        Snackbar.make(view, message, Snackbar.LENGTH_LONG).show();
    }

    /**
     * Gets the current network connectivity status
     *
     * @return int current network connectivity status
     */
    public int getConnectivityStatus() {
        ConnectivityManager cm = (ConnectivityManager) mContext
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (activeNetwork != null) {
            if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI)
                return TYPE_WIFI;

            if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE)
                return TYPE_MOBILE;
        }
        return TYPE_NOT_CONNECTED;
    }

    /**
     * Convert byte array to hex string
     *
     * @param bytes byte[]
     * @return String
     */
    public String bytesToHex(byte[] bytes) {
        StringBuilder sbuf = new StringBuilder();
        for (byte aByte : bytes) {
            int intVal = aByte & 0xff;
            if (intVal < 0x10) sbuf.append("0");
            sbuf.append(Integer.toHexString(intVal).toUpperCase());
        }
        return sbuf.toString();
    }

    /**
     * Get utf8 byte array.
     *
     * @param str String
     * @return array of NULL if error was found
     */
    public byte[] getUTF8Bytes(String str) {
        try {
            return str.getBytes("UTF-8");
        } catch (Exception ex) {
            return null;
        }
    }

    /**
     * Load UTF8withBOM or any ansi text file.
     *
     * @param filename String
     * @return String
     * @throws java.io.IOException Exception
     */
    public String loadFileAsString(String filename) throws java.io.IOException {
        final int BUFLEN = 1024;
        BufferedInputStream is = new BufferedInputStream(new FileInputStream(filename), BUFLEN);
        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream(BUFLEN);
            byte[] bytes = new byte[BUFLEN];
            boolean isUTF8 = false;
            int read, count = 0;
            while ((read = is.read(bytes)) != -1) {
                if (count == 0 && bytes[0] == (byte) 0xEF && bytes[1] == (byte) 0xBB && bytes[2] == (byte) 0xBF) {
                    isUTF8 = true;
                    baos.write(bytes, 3, read - 3); // drop UTF8 bom marker
                } else {
                    baos.write(bytes, 0, read);
                }
                count += read;
            }
            return isUTF8 ? new String(baos.toByteArray(), "UTF-8") : new String(baos.toByteArray());
        } finally {
            try {
                is.close();
            } catch (Exception ignored) {
            }
        }
    }

    /**
     * Returns MAC address of the given interface name.
     *
     * @param interfaceName eth0, wlan0 or NULL=use first interface
     * @return mac address or empty string
     */
    public String getMACAddress(String interfaceName) {
        try {
            List<NetworkInterface> interfaces = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface intf : interfaces) {
                if (interfaceName != null) {
                    if (!intf.getName().equalsIgnoreCase(interfaceName)) continue;
                }
                byte[] mac = intf.getHardwareAddress();
                if (mac == null) return "";
                StringBuilder buf = new StringBuilder();
                for (byte aMac : mac) buf.append(String.format("%02X:", aMac));
                if (buf.length() > 0) buf.deleteCharAt(buf.length() - 1);
                return buf.toString();
            }
        } catch (Exception ignored) {
        } // for now eat exceptions
        return "";
        /*try {
            // this is so Linux hack
            return loadFileAsString("/sys/class/net/" +interfaceName + "/address").toUpperCase().trim();
        } catch (IOException ex) {
            return null;
        }*/
    }

    /**
     * Get IP address from first non-localhost interface
     *
     * @param useIPv4 true=return ipv4, false=return ipv6
     * @return address or empty string
     */
    public String getIPAddress(boolean useIPv4) {
        try {
            List<NetworkInterface> interfaces = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface intf : interfaces) {
                List<InetAddress> addrs = Collections.list(intf.getInetAddresses());
                for (InetAddress addr : addrs) {
                    if (!addr.isLoopbackAddress()) {
                        String sAddr = addr.getHostAddress();
                        //boolean isIPv4 = InetAddressUtils.isIPv4Address(sAddr);
                        boolean isIPv4 = sAddr.indexOf(':') < 0;

                        if (useIPv4) {
                            if (isIPv4)
                                return sAddr;
                        } else {
                            if (!isIPv4) {
                                int delim = sAddr.indexOf('%'); // drop ip6 zone suffix
                                return delim < 0 ? sAddr.toUpperCase() : sAddr.substring(0, delim).toUpperCase();
                            }
                        }
                    }
                }
            }
        } catch (Exception ignored) {
        } // for now eat exceptions
        return "";
    }
}
