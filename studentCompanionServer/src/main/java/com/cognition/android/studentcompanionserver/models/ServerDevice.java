package com.cognition.android.studentcompanionserver.models;


import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.structure.BaseModel;

@Table(database = AppDatabase.class, allFields = true)
public class ServerDevice extends BaseModel {

    @PrimaryKey
    private Integer id;
    private String ssid;
    private String macAddress;
    private String faToken;
    private String authToken;

    /**
     * No args constructor for use in serialization
     */
    public ServerDevice() {
    }

    /**
     * Constructor without id
     *
     * @param ssid       String
     * @param macAddress String
     * @param faToken    String
     */
    public ServerDevice(String ssid, String macAddress, String faToken) {
        super();
        this.ssid = ssid;
        this.macAddress = macAddress;
        this.faToken = faToken;
    }

    public ServerDevice(String ssid, String macAddress, String faToken, String authToken) {
        super();
        this.ssid = ssid;
        this.macAddress = macAddress;
        this.faToken = faToken;
        this.authToken = authToken;
    }

    /**
     * Constructor
     *
     * @param id         Integer
     * @param ssid       String
     * @param macAddress String
     * @param faToken    String
     */
    public ServerDevice(Integer id, String ssid, String macAddress, String faToken) {
        super();
        this.id = id;
        this.ssid = ssid;
        this.macAddress = macAddress;
        this.faToken = faToken;
    }

    public ServerDevice(Integer id, String ssid, String macAddress, String faToken, String authToken) {
        super();
        this.id = id;
        this.ssid = ssid;
        this.macAddress = macAddress;
        this.faToken = faToken;
        this.authToken = authToken;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSsid() {
        return ssid;
    }

    public void setSsid(String ssid) {
        this.ssid = ssid;
    }

    public String getMacAddress() {
        return macAddress;
    }

    public void setMacAddress(String macAddress) {
        this.macAddress = macAddress;
    }

    public String getFaToken() {
        return faToken;
    }

    public void setFaToken(String faToken) {
        this.faToken = faToken;
    }

    public String getAuthToken() {
        return authToken;
    }

    public void setAuthToken(String authToken) {
        this.authToken = authToken;
    }

    public com.cognition.android.studentcompanionserver.pojo.ServerDevice getPojo() {
        return new com.cognition.android.studentcompanionserver.pojo.ServerDevice(
                this.id, this.ssid, this.macAddress, this.faToken, this.authToken
        );
    }

    @Override
    public String toString() {
        return String.format("SSID: %s\tMAC: %s\tToken: %s", this.ssid, this.macAddress, this.faToken);
    }
}

