package com.cognition.android.studentcompanionserver.models;

import com.raizlabs.android.dbflow.annotation.Database;

/**
 * Application Database class
 */

@Database(name = AppDatabase.NAME, version = AppDatabase.VERSION)
public abstract class AppDatabase {

    public static final String NAME = "AppDatabase";

    public static final int VERSION = 1;
}