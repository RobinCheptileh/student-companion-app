package com.cognition.android.studentcompanionserver.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;

import com.cognition.android.studentcompanionserver.R;
import com.cognition.android.studentcompanionserver.hotspotmanager.WifiApManager;
import com.cognition.android.studentcompanionserver.pojo.ServerDevice;
import com.cognition.android.studentcompanionserver.pojo.Student;
import com.cognition.android.studentcompanionserver.pojo.Token;
import com.cognition.android.studentcompanionserver.utils.ApiClient;
import com.cognition.android.studentcompanionserver.utils.ApiInterface;
import com.cognition.android.studentcompanionserver.utils.Utils;
import com.google.android.material.textfield.TextInputLayout;
import com.wang.avi.AVLoadingIndicatorView;

import java.io.IOException;
import java.util.List;
import java.util.regex.Pattern;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.transition.TransitionManager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {

    ConstraintLayout lytLoginParent;
    TextInputLayout txtUsernameWrap;
    AppCompatEditText txtUsername;
    TextInputLayout txtPasswordWrap;
    AppCompatEditText txtPassword;
    ViewGroup lytLoginButtonAnimContainer;
    AppCompatButton btnLogin;
    AVLoadingIndicatorView animLoginLoadIndicator;

    SharedPreferences sharedPref;
    Utils mUtils;
    WifiApManager wifiApManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.LoginTheme);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        sharedPref = LoginActivity.this.getSharedPreferences(getString(R.string.preferences_file_name), Context.MODE_PRIVATE);
        mUtils = new Utils(LoginActivity.this);
        wifiApManager = new WifiApManager(LoginActivity.this);

        initViews();
    }

    /**
     * Initialize views
     */
    private void initViews() {
        lytLoginParent = findViewById(R.id.lytLoginParent);
        txtUsernameWrap = findViewById(R.id.txtLoginUsernameWrap);
        txtUsername = findViewById(R.id.txtLoginUsername);
        txtPasswordWrap = findViewById(R.id.txtPasswordWrap);
        txtPassword = findViewById(R.id.txtPassword);
        lytLoginButtonAnimContainer = findViewById(R.id.lytLoginButtonAnimContainer);
        btnLogin = findViewById(R.id.btnLogin);
        animLoginLoadIndicator = findViewById(R.id.animLoginLoadIndicator);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String username = txtUsername.getText().toString().toUpperCase().trim().replaceAll(Pattern.quote("/"), "_");
                String password = txtPassword.getText().toString().trim();
                if (username.isEmpty())
                    txtUsernameWrap.setError(getString(R.string.field_is_empty));
                else if (password.isEmpty())
                    txtPasswordWrap.setError(getString(R.string.field_is_empty));
                else {
                    if (mUtils.getConnectivityStatus() != Utils.TYPE_NOT_CONNECTED) {
                        TransitionManager.beginDelayedTransition(lytLoginButtonAnimContainer);
                        btnLogin.setVisibility(View.GONE);
                        animLoginLoadIndicator.setVisibility(View.VISIBLE);

                        login(username, password);
                    } else
                        mUtils.showSnackbar(lytLoginParent, getString(R.string.an_error_occurred));
                }
                Log.d(MainActivity.TAG, username);
            }
        });
    }

    /**
     * Perform login call to server
     *
     * @param reg_no   String
     * @param password String
     */
    private void login(final String reg_no, String password) {
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<Token> call = apiInterface.getToken(reg_no, password);
        call.enqueue(new Callback<Token>() {
            @Override
            public void onResponse(@NonNull Call<Token> call, @NonNull Response<Token> response) {
                if (response.isSuccessful() && response.body() != null) {
                    SharedPreferences.Editor editor = sharedPref.edit();
                    editor.putString(
                            getString(R.string.auth_token_name),
                            String.format("Bearer %s", response.body().getToken())
                    );
                    editor.apply();
                    Log.d(MainActivity.TAG, String.format("Login success. Token %s", response.body().getToken()));

                    checkIfStaff(reg_no);
                } else {
                    try {
                        if (response.code() == 400) {
                            Log.d(MainActivity.TAG, response.errorBody().string());
                            TransitionManager.beginDelayedTransition(lytLoginButtonAnimContainer);
                            btnLogin.setVisibility(View.VISIBLE);
                            animLoginLoadIndicator.setVisibility(View.GONE);

                            txtUsernameWrap.setError(getString(R.string.invalid_reg_no_or_pass));
                        } else {
                            Log.d(MainActivity.TAG, response.errorBody().string());
                            TransitionManager.beginDelayedTransition(lytLoginButtonAnimContainer);
                            btnLogin.setVisibility(View.VISIBLE);
                            animLoginLoadIndicator.setVisibility(View.GONE);
                            mUtils.showSnackbar(lytLoginParent, getString(R.string.an_error_occurred));
                        }
                    } catch (IOException e) {
                        Log.e(MainActivity.TAG, e.toString());
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<Token> call, @NonNull Throwable t) {
                Log.e(MainActivity.TAG, t.toString());
                TransitionManager.beginDelayedTransition(lytLoginButtonAnimContainer);
                btnLogin.setVisibility(View.VISIBLE);
                animLoginLoadIndicator.setVisibility(View.GONE);
                mUtils.showSnackbar(lytLoginParent, getString(R.string.an_error_occurred));
            }
        });
    }

    /**
     * Check if the user is a staff
     *
     * @param username String
     */
    private void checkIfStaff(String username) {
        String authToken = sharedPref.getString(getString(R.string.auth_token_name), null);
        if (authToken != null) {
            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<Student> call = apiInterface.getStudentDetails(authToken, username);
            call.enqueue(new Callback<Student>() {
                @Override
                public void onResponse(@NonNull Call<Student> call, @NonNull Response<Student> response) {
                    if (response.isSuccessful() && response.body() != null) {
                        if (response.body().getIsStaff()) {
                            Log.d(MainActivity.TAG, String.format("%s is staff", response.body().getRegistrationNo()));
                            checkDeviceRegistration();
                        } else {
                            Log.d(MainActivity.TAG, String.format("%s is not staff", response.body().getRegistrationNo()));
                            TransitionManager.beginDelayedTransition(lytLoginButtonAnimContainer);
                            btnLogin.setVisibility(View.VISIBLE);
                            animLoginLoadIndicator.setVisibility(View.GONE);

                            txtUsernameWrap.setError(getString(R.string.invalid_reg_no_or_pass));
                        }
                    } else {
                        try {
                            if (response.code() == 400) {
                                Log.d(MainActivity.TAG, response.errorBody().string());
                                TransitionManager.beginDelayedTransition(lytLoginButtonAnimContainer);
                                btnLogin.setVisibility(View.VISIBLE);
                                animLoginLoadIndicator.setVisibility(View.GONE);

                                txtUsernameWrap.setError(getString(R.string.invalid_reg_no_or_pass));
                            } else {
                                Log.d(MainActivity.TAG, response.errorBody().string());
                                TransitionManager.beginDelayedTransition(lytLoginButtonAnimContainer);
                                btnLogin.setVisibility(View.VISIBLE);
                                animLoginLoadIndicator.setVisibility(View.GONE);
                                mUtils.showSnackbar(lytLoginParent, getString(R.string.an_error_occurred));
                            }
                        } catch (IOException e) {
                            Log.e(MainActivity.TAG, e.toString());
                        }
                    }
                }

                @Override
                public void onFailure(@NonNull Call<Student> call, @NonNull Throwable t) {
                    Log.e(MainActivity.TAG, t.toString());
                    TransitionManager.beginDelayedTransition(lytLoginButtonAnimContainer);
                    btnLogin.setVisibility(View.VISIBLE);
                    animLoginLoadIndicator.setVisibility(View.GONE);
                    mUtils.showSnackbar(lytLoginParent, getString(R.string.an_error_occurred));
                }
            });
        } else {
            Log.e(MainActivity.TAG, getString(R.string.missing_token_error));
            TransitionManager.beginDelayedTransition(lytLoginButtonAnimContainer);
            btnLogin.setVisibility(View.VISIBLE);
            animLoginLoadIndicator.setVisibility(View.GONE);
            mUtils.showSnackbar(lytLoginParent, getString(R.string.an_error_occurred));
        }
    }

    /**
     * Check if device is registered
     */
    private void checkDeviceRegistration() {
        String authToken = sharedPref.getString(getString(R.string.auth_token_name), null);
        if (authToken != null) {
            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<List<ServerDevice>> call = apiInterface.listServerDevices(authToken);
            call.enqueue(new Callback<List<ServerDevice>>() {
                @Override
                public void onResponse(@NonNull Call<List<ServerDevice>> call, @NonNull Response<List<ServerDevice>> response) {
                    if (response.isSuccessful() && response.body() != null) {
                        for (ServerDevice serverDevice : response.body()) {
                            if (mUtils.getMACAddress("wlan0").equals(serverDevice.getMacAddress())) {
                                Log.d(MainActivity.TAG, String.format("Device is registered. ID: %d", serverDevice.getId()));
                                SharedPreferences.Editor editor = sharedPref.edit();
                                editor.putInt(
                                        getString(R.string.device_id_name),
                                        serverDevice.getId()
                                );
                                editor.apply();
                                serverDevice.getModel().save();
                                Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(intent);
                                finish();
                                return;
                            }
                        }
                        Log.d(MainActivity.TAG, "Device is not registered");
                        registerDevice();
                    } else {
                        try {
                            Log.e(MainActivity.TAG, response.errorBody().string());
                        } catch (IOException e) {
                            Log.e(MainActivity.TAG, e.toString());
                        }
                        TransitionManager.beginDelayedTransition(lytLoginButtonAnimContainer);
                        btnLogin.setVisibility(View.VISIBLE);
                        animLoginLoadIndicator.setVisibility(View.GONE);
                        mUtils.showSnackbar(lytLoginParent, getString(R.string.an_error_occurred));
                    }
                }

                @Override
                public void onFailure(@NonNull Call<List<ServerDevice>> call, @NonNull Throwable t) {
                    Log.d(MainActivity.TAG, t.toString());
                    TransitionManager.beginDelayedTransition(lytLoginButtonAnimContainer);
                    btnLogin.setVisibility(View.VISIBLE);
                    animLoginLoadIndicator.setVisibility(View.GONE);
                    mUtils.showSnackbar(lytLoginParent, getString(R.string.an_error_occurred));
                }
            });
        } else {
            Log.e(MainActivity.TAG, getString(R.string.missing_token_error));
            TransitionManager.beginDelayedTransition(lytLoginButtonAnimContainer);
            btnLogin.setVisibility(View.VISIBLE);
            animLoginLoadIndicator.setVisibility(View.GONE);
            mUtils.showSnackbar(lytLoginParent, getString(R.string.an_error_occurred));
        }
    }

    /**
     * Register current device
     */
    private void registerDevice() {
        String authToken = sharedPref.getString(getString(R.string.auth_token_name), null);
        String faToken = sharedPref.getString(getString(R.string.firebase_token_name), "");
        if (authToken != null && faToken != null) {
            ServerDevice newServerDevice = new ServerDevice(
                    wifiApManager.getWifiApConfiguration().SSID.replace("\"", ""),
                    mUtils.getMACAddress("wlan0"),
                    faToken
            );
            Log.d(MainActivity.TAG, String.format("About to register: %s", newServerDevice.toString()));
            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<ServerDevice> call = apiInterface.registerServerDevice(
                    authToken,
                    newServerDevice
            );
            call.enqueue(new Callback<ServerDevice>() {
                @Override
                public void onResponse(@NonNull Call<ServerDevice> call, @NonNull Response<ServerDevice> response) {
                    if (response.isSuccessful() && response.body() != null) {
                        SharedPreferences.Editor editor = sharedPref.edit();
                        editor.putInt(
                                getString(R.string.device_id_name),
                                response.body().getId()
                        );
                        editor.apply();
                        response.body().getModel().save();
                        Log.d(MainActivity.TAG, String.format("Device registered successfully. ID: %d", response.body().getId()));
                        /*if (mUtils.addServerDeviceData(response.body()))
                            Log.d(MainActivity.TAG, String.format("Device %d added to Firestore successfully", response.body().getId()));
                        else
                            Log.e(MainActivity.TAG, String.format("Failed to add device %d to Firestore", response.body().getId()));*/
                        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        finish();
                    } else {
                        try {
                            Log.e(MainActivity.TAG, response.errorBody().string());
                        } catch (IOException e) {
                            Log.e(MainActivity.TAG, e.toString());
                        }
                        TransitionManager.beginDelayedTransition(lytLoginButtonAnimContainer);
                        btnLogin.setVisibility(View.VISIBLE);
                        animLoginLoadIndicator.setVisibility(View.GONE);
                        mUtils.showSnackbar(lytLoginParent, getString(R.string.an_error_occurred));
                    }
                }

                @Override
                public void onFailure(@NonNull Call<ServerDevice> call, @NonNull Throwable t) {
                    Log.d(MainActivity.TAG, t.toString());
                    TransitionManager.beginDelayedTransition(lytLoginButtonAnimContainer);
                    btnLogin.setVisibility(View.VISIBLE);
                    animLoginLoadIndicator.setVisibility(View.GONE);
                    mUtils.showSnackbar(lytLoginParent, getString(R.string.an_error_occurred));
                }
            });
        } else {
            Log.e(MainActivity.TAG, getString(R.string.missing_token_error));
            TransitionManager.beginDelayedTransition(lytLoginButtonAnimContainer);
            btnLogin.setVisibility(View.VISIBLE);
            animLoginLoadIndicator.setVisibility(View.GONE);
            mUtils.showSnackbar(lytLoginParent, getString(R.string.an_error_occurred));
        }
    }
}
