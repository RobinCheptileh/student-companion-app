package com.cognition.android.studentcompanionserver.utils;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import com.cognition.android.studentcompanionserver.R;
import com.cognition.android.studentcompanionserver.hotspotmanager.ClientScanResult;

import java.util.ArrayList;
import java.util.List;

/**
 * Adapter class
 */

public class IpsAdapter extends RecyclerView.Adapter<IpsAdapter.IpViewHolder> implements Filterable {
    private List<ClientScanResult> ipsList;
    private List<ClientScanResult> filteredIpList;

    public class IpViewHolder extends RecyclerView.ViewHolder {
        private AppCompatTextView txtIpAddress, txtMacAddress;

        public IpViewHolder(View view) {
            super(view);

            txtIpAddress = view.findViewById(R.id.txtIpAddress);
            txtMacAddress = view.findViewById(R.id.txtMacAddress);
        }
    }

    public IpsAdapter(List<ClientScanResult> ipsList) {
        this.ipsList = ipsList;
        this.filteredIpList = ipsList;
    }

    @NonNull
    @Override
    public IpViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.ip_list_row, parent, false);

        return new IpViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull IpViewHolder holder, int position) {
        ClientScanResult clientScanResult = this.filteredIpList.get(position);
        holder.txtIpAddress.setText(clientScanResult.getIpAddr());
        holder.txtMacAddress.setText(clientScanResult.getHWAddr());
    }

    @Override
    public int getItemCount() {
        return this.filteredIpList.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                String searchText = constraint.toString().trim();
                if (searchText.isEmpty())
                    filteredIpList = ipsList;
                else {
                    List<ClientScanResult> newFilteredList = new ArrayList<>();
                    for (ClientScanResult client : ipsList)
                        if (client.getIpAddr().toLowerCase().contains(searchText) || client.getHWAddr().toLowerCase().contains(searchText))
                            newFilteredList.add(client);
                    filteredIpList = newFilteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = filteredIpList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                filteredIpList = (ArrayList<ClientScanResult>) results.values;
                notifyDataSetChanged();
            }
        };
    }
}
