package com.cognition.android.studentcompanionserver.firebase;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.cognition.android.studentcompanionserver.activities.MainActivity;
import com.cognition.android.studentcompanionserver.R;
import com.cognition.android.studentcompanionserver.utils.Utils;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {

    public MyFirebaseInstanceIDService() {
    }

    @Override
    public void onTokenRefresh() {
        Utils mUtils = new Utils(this);
        SharedPreferences sharedPref = getApplicationContext().getSharedPreferences(getString(R.string.preferences_file_name), Context.MODE_PRIVATE);

        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d(MainActivity.TAG, String.format("Refreshed token: %s", refreshedToken));

        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(
                getString(R.string.firebase_token_name),
                refreshedToken
        );
        editor.apply();

        mUtils.updateFirebaseToken(refreshedToken);
    }
}
