package com.cognition.android.studentcompanionserver.activities;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.net.wifi.WifiConfiguration;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.cognition.android.studentcompanionserver.BuildConfig;
import com.cognition.android.studentcompanionserver.R;
import com.cognition.android.studentcompanionserver.firebase.MyFirebaseInstanceIDService;
import com.cognition.android.studentcompanionserver.firebase.MyFirebaseMessagingService;
import com.cognition.android.studentcompanionserver.hotspotmanager.ClientScanResult;
import com.cognition.android.studentcompanionserver.hotspotmanager.FinishScanListener;
import com.cognition.android.studentcompanionserver.hotspotmanager.WifiApManager;
import com.cognition.android.studentcompanionserver.pojo.ServerDevice;
import com.cognition.android.studentcompanionserver.utils.ApiClient;
import com.cognition.android.studentcompanionserver.utils.ApiInterface;
import com.cognition.android.studentcompanionserver.utils.IpsAdapter;
import com.cognition.android.studentcompanionserver.utils.Utils;
import com.getkeepsafe.taptargetview.TapTarget;
import com.getkeepsafe.taptargetview.TapTargetSequence;
import com.getkeepsafe.taptargetview.TapTargetView;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.iid.FirebaseInstanceId;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatCheckBox;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    CollapsingToolbarLayout collapsingToolbarLayout;
    AppBarLayout appBarLayout;
    Toolbar toolbar;
    DrawerLayout mDrawerLayout;
    AppCompatTextView txtNavHeaderSsidName;
    AppCompatTextView txtDeviceCount;
    SwipeRefreshLayout refreshList;
    RecyclerView lstIps;
    FloatingActionButton fabToggleHotspot;

    WifiApManager wifiApManager;
    WifiConfiguration mWifiConfiguration;
    IpsAdapter mIpsAdapter;

    boolean ssidUpdate = false;

    /**
     * App tag for Logging
     */
    public static final String TAG = "StudentCompanionServer";

    /**
     * Value referenced in onActivityResult after requesting google play services
     */
    public static final int REQUEST_GOOGLE_PLAY_SERVICES = 1989;

    /**
     * Firebase analytics instance
     */
    private FirebaseAnalytics mFirebaseAnalytics;

    /**
     * Shared Preferences Instance
     */
    SharedPreferences sharedPref;

    /**
     * General utilities instance
     */
    private Utils mUtils;

    /**
     * If the wifi network should be hidden or discoverable
     */
    boolean isHidden = false;

    Timer timer;

    /**
     * Method called when activity is created
     *
     * @param savedInstanceState Bundle
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mFirebaseAnalytics = FirebaseAnalytics.getInstance(MainActivity.this);

        sharedPref = MainActivity.this.getSharedPreferences(getString(R.string.preferences_file_name), Context.MODE_PRIVATE);

        mUtils = new Utils(MainActivity.this);

        checkGooglePlayCompat();
        confirmFATokenUpdate();
        initViews();
        scanIps();
        discoverFeatures();

        timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
                                      @Override
                                      public void run() {
                                          runOnUiThread(new Runnable() {
                                              @Override
                                              public void run() {
                                                  scanIps();
                                              }
                                          });
                                      }
                                  },
                0, 1000);
    }

    /**
     * Method called when activity is paused
     */
    @Override
    protected void onPause() {
        super.onPause();
    }

    /**
     * Method called when activity is resumed
     */
    @Override
    protected void onResume() {
        super.onResume();
        checkGooglePlayCompat();
    }

    /**
     * Method called when activity is stopped
     */
    @Override
    protected void onStop() {
        super.onStop();
        timer.cancel();
    }

    /**
     * Method called when activity is destroyed
     */
    @Override
    protected void onDestroy() {
        super.onDestroy();
        timer.cancel();
    }

    /**
     * Method called when back key is pressed
     */
    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    /**
     * Method called when menu options are created
     *
     * @param menu Menu
     * @return boolean
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    /**
     * Method called when menu item is selected
     *
     * @param item MenuItem
     * @return boolean
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    /**
     * Method called when activity gain result from another activity
     *
     * @param requestCode int
     * @param resultCode  int
     * @param data        Intent
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_GOOGLE_PLAY_SERVICES:
                if (resultCode == Activity.RESULT_OK) {
                    Intent firebaseInstanceIDService = new Intent(MainActivity.this, MyFirebaseInstanceIDService.class);
                    startService(firebaseInstanceIDService);

                    Intent firebaseMessagingService = new Intent(MainActivity.this, MyFirebaseMessagingService.class);
                    startService(firebaseMessagingService);
                }
                break;
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    /**
     * Initialize the views in the activity
     */
    private void initViews() {
        collapsingToolbarLayout = findViewById(R.id.collapsing_toolbar);

        appBarLayout = findViewById(R.id.app_bar_layout);

        toolbar = findViewById(R.id.toolbar);
        toolbar.inflateMenu(R.menu.main_menu);
        toolbar.setTitle(R.string.empty_text);
        toolbar.setNavigationIcon(R.drawable.ic_menu);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDrawerLayout.openDrawer(GravityCompat.START);
            }
        });

        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView = (SearchView) toolbar.getMenu().findItem(R.id.action_search).getActionView();
        searchView.setQueryHint("Search");
        searchView.setSearchableInfo(searchManager != null ? searchManager.getSearchableInfo(getComponentName()) : null);

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                mIpsAdapter.getFilter().filter(query);

                Bundle bundle = new Bundle();
                bundle.putString(FirebaseAnalytics.Param.SEARCH_TERM, query);
                mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SEARCH, bundle);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                mIpsAdapter.getFilter().filter(newText);

                Bundle bundle = new Bundle();
                bundle.putString(FirebaseAnalytics.Param.SEARCH_TERM, newText);
                mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SEARCH, bundle);
                return true;
            }
        });

        searchView.setOnQueryTextFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus)
                    appBarLayout.setExpanded(false);
            }
        });
        // setSupportActionBar(toolbar);
        // ActionBar actionbar = getSupportActionBar();
        /*if (actionbar != null && homeDrawable != null) {
            actionbar.setDisplayHomeAsUpEnabled(true);
            actionbar.setDisplayShowTitleEnabled(false);
            homeDrawable = DrawableCompat.wrap(homeDrawable);
            DrawableCompat.setTint(homeDrawable, getResources().getColor(android.R.color.white));
            actionbar.setHomeAsUpIndicator(homeDrawable);
        }*/

        mDrawerLayout = findViewById(R.id.drawer_layout);
        refreshList = findViewById(R.id.refreshList);
        lstIps = findViewById(R.id.lstIps);
        fabToggleHotspot = findViewById(R.id.fabToggleHotspot);

        // collapsingToolbarLayout.setTitle(getResources().getString(R.string.app_name));
        collapsingToolbarLayout.setExpandedTitleColor(getResources().getColor(android.R.color.transparent));
        collapsingToolbarLayout.setContentScrimColor(getResources().getColor(R.color.colorPrimary));
        collapsingToolbarLayout.setStatusBarScrimColor(getResources().getColor(R.color.colorPrimaryDark));
        mDrawerLayout.addDrawerListener(
                new DrawerLayout.DrawerListener() {
                    @Override
                    public void onDrawerSlide(@NonNull View drawerView, float slideOffset) {
                        // Respond when the drawer's position changes
                    }

                    @Override
                    public void onDrawerOpened(@NonNull View drawerView) {
                        // Respond when the drawer is opened
                    }

                    @Override
                    public void onDrawerClosed(@NonNull View drawerView) {
                        // Respond when the drawer is closed
                    }

                    @Override
                    public void onDrawerStateChanged(int newState) {
                        // Respond when the drawer motion state changes
                    }
                }
        );

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                        // set item as selected to persist highlight
                        // menuItem.setChecked(true);
                        // close drawer when item is tapped
                        mDrawerLayout.closeDrawers();

                        switch (menuItem.getItemId()) {
                            case R.id.edit_ssid:
                                showChangeSsidDialog();
                                break;
                            case R.id.refresh_list:
                                scanIps();
                                break;
                            case R.id.sync_db:
                                scanIps();
                                break;
                            case R.id.about:
                                mUtils.showSnackbar(refreshList, String.format(
                                        "%s version %s",
                                        getResources().getString(R.string.app_name),
                                        BuildConfig.VERSION_NAME
                                ));
                                break;
                        }

                        return true;
                    }
                });
        txtNavHeaderSsidName = navigationView.getHeaderView(0).findViewById(R.id.txtNavHeaderSsidName);
        txtDeviceCount = findViewById(R.id.txtDeviceCount);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(MainActivity.this);
        lstIps.setLayoutManager(mLayoutManager);
        lstIps.setItemAnimator(new DefaultItemAnimator());

        refreshList.setColorSchemeResources(
                R.color.colorLogoBlue, R.color.colorLogoPink, R.color.colorLogoGold
        );
        refreshList.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                scanIps();
            }
        });

        wifiApManager = new WifiApManager(MainActivity.this);
        wifiApManager.showWritePermissionSettings(true);

        mWifiConfiguration = mUtils.setWifiConfig("Identifying...", "Coloud#E6410", true);

        fabToggleHotspot.setRippleColor(getResources().getColor(R.color.colorAccentPressed));
        if (wifiApManager.isWifiApEnabled()) {
            txtNavHeaderSsidName.setText(String.format("Current SSID: %s", wifiApManager.getWifiApConfiguration().SSID));
            fabToggleHotspot.setImageResource(R.drawable.ic_wifi_tethering_white_24dp);
        } else {
            txtNavHeaderSsidName.setText(R.string.hotspot_off);
            fabToggleHotspot.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(android.R.color.darker_gray)));
            fabToggleHotspot.setImageResource(R.drawable.ic_portable_wifi_off_white_24dp);
        }

        fabToggleHotspot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateFab(wifiApManager.isWifiApEnabled());
            }
        });
    }

    /**
     * Check if google play services is installed and compatible
     */
    private void checkGooglePlayCompat() {
        GoogleApiAvailability googleApiAvailability = GoogleApiAvailability.getInstance();
        int result = googleApiAvailability.isGooglePlayServicesAvailable(MainActivity.this);
        if (result != ConnectionResult.SUCCESS) {
            if (googleApiAvailability.isUserResolvableError(result)) {
                googleApiAvailability.showErrorDialogFragment(
                        MainActivity.this,
                        result,
                        REQUEST_GOOGLE_PLAY_SERVICES,
                        new DialogInterface.OnCancelListener() {
                            @Override
                            public void onCancel(DialogInterface dialog) {
                                mUtils.showSnackbar(refreshList, getResources().getString(R.string.missing_gps_warning));
                            }
                        }
                );
            } else {
                mUtils.showSnackbar(refreshList, getResources().getString(R.string.missing_gps_warning));
            }
        }
    }

    /**
     * Confirm is Firebase token is updated on server and check if
     * device was added to Firestore
     */
    private void confirmFATokenUpdate() {
        boolean faTokenUpdateSuccess = sharedPref.getBoolean(getString(R.string.fa_token_update_success_name), false);

        if (!faTokenUpdateSuccess)
            mUtils.updateFirebaseToken(FirebaseInstanceId.getInstance().getToken());
    }

    /**
     * Update the ssid on server
     */
    private boolean updateSsidOnServer() {
        ssidUpdate = false;
        String authToken = sharedPref.getString(getString(R.string.auth_token_name), null);
        int deviceID = sharedPref.getInt(getString(R.string.device_id_name), 0);
        if (authToken != null && deviceID != 0) {
            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<ServerDevice> call = apiInterface.updateServerSsid(authToken, deviceID, wifiApManager.getWifiApConfiguration().SSID.replace("\"", ""));
            call.enqueue(new Callback<ServerDevice>() {
                @Override
                public void onResponse(@NonNull Call<ServerDevice> call, @NonNull Response<ServerDevice> response) {
                    if (response.isSuccessful() && response.body() != null) {
                        Log.d(MainActivity.TAG, "ssid successfully updated");
                        ssidUpdate = true;
                    } else {
                        try {
                            Log.e(MainActivity.TAG, response.errorBody().string());
                        } catch (IOException e) {
                            Log.e(MainActivity.TAG, e.toString());
                        }
                        mUtils.showSnackbar(refreshList, getString(R.string.an_error_occurred));
                    }
                }

                @Override
                public void onFailure(@NonNull Call<ServerDevice> call, @NonNull Throwable t) {
                    Log.d(MainActivity.TAG, t.toString());
                    mUtils.showSnackbar(refreshList, getString(R.string.an_error_occurred));
                }
            });
        } else {
            Log.e(MainActivity.TAG, getString(R.string.missing_token_error));
            mUtils.showSnackbar(refreshList, getString(R.string.an_error_occurred));
        }

        return ssidUpdate;
    }

    /**
     * Scan for connected IPs
     */
    private void scanIps() {
        refreshList.setRefreshing(true);
        wifiApManager.getClientList(true, new FinishScanListener() {
            @Override
            public void onFinishScan(final ArrayList<ClientScanResult> clients) {
                mIpsAdapter = new IpsAdapter(clients);
                lstIps.setAdapter(mIpsAdapter);
                mIpsAdapter.notifyDataSetChanged();
                txtDeviceCount.setText(String.valueOf(clients.size()));
                refreshList.setRefreshing(false);
                mUtils.updateConnectedDevicesToFS(clients);
            }
        });
    }

    /**
     * Display feature discovery
     */
    private void discoverFeatures() {
        TapTargetSequence sequence = new TapTargetSequence(MainActivity.this)
                .targets(
                        TapTarget
                                .forToolbarNavigationIcon(toolbar, "More actions!", "Open to view available actions").id(1)
                                .drawShadow(true),
                        TapTarget
                                .forToolbarMenuItem(toolbar, R.id.action_search, "Search", "Easily find connected devices").id(2)
                                .drawShadow(true),
                        TapTarget.
                                forView(fabToggleHotspot, "Toggle hotspot state", "Tap to switch wifi hotpot on/off")
                                .drawShadow(true)
                                .icon(getResources().getDrawable(R.drawable.ic_wifi_tethering_white_24dp))
                )
                .listener(new TapTargetSequence.Listener() {
                    // This listener will tell us when interesting(tm) events happen in regards
                    // to the sequence
                    @Override
                    public void onSequenceFinish() {
                        mUtils.showSnackbar(refreshList, "You're good to go!");
                    }

                    @Override
                    public void onSequenceStep(TapTarget lastTarget, boolean targetClicked) {
                        Log.d(TAG, "Clicked on " + lastTarget.id());
                    }

                    @Override
                    public void onSequenceCanceled(TapTarget lastTarget) {
                        final AlertDialog dialog = new AlertDialog.Builder(MainActivity.this)
                                .setTitle("Uh oh")
                                .setMessage("You canceled the sequence")
                                .setPositiveButton("Oops", null).show();
                        TapTargetView.showFor(dialog,
                                TapTarget.forView(dialog.getButton(DialogInterface.BUTTON_POSITIVE), "Uh oh!", "You canceled the sequence at step " + lastTarget.id())
                                        .cancelable(false)
                                        .tintTarget(false), new TapTargetView.Listener() {
                                    @Override
                                    public void onTargetClick(TapTargetView view) {
                                        super.onTargetClick(view);
                                        dialog.dismiss();
                                    }
                                });
                    }
                });
        sequence.considerOuterCircleCanceled(true);
        sequence.continueOnCancel(true);
        sequence.start();
    }

    /**
     * Update Hotspot toggle FAB
     *
     * @param hotspotState boolean
     */
    private void updateFab(boolean hotspotState) {
        if (hotspotState) {
            wifiApManager.setWifiApEnabled(null, false);
            fabToggleHotspot.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(android.R.color.darker_gray)));
            fabToggleHotspot.setImageResource(R.drawable.ic_portable_wifi_off_white_24dp);
        } else {
            wifiApManager.setWifiApEnabled(null, true);
            fabToggleHotspot.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.colorAccent)));
            fabToggleHotspot.setImageResource(R.drawable.ic_wifi_tethering_white_24dp);
        }

        scanIps();
    }

    /**
     * Show change SSID dialog
     */
    @SuppressLint("InflateParams")
    private void showChangeSsidDialog() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        // Get the layout inflater
        LayoutInflater inflater = getLayoutInflater();
        View promptView = inflater.inflate(R.layout.ssid_prompt_layout, null);

        final TextInputLayout txtSsidWrap = promptView.findViewById(R.id.txtSsidWrap);
        final AppCompatEditText txtSsid = promptView.findViewById(R.id.txtSsid);
        final TextInputLayout txtPasswordWrap = promptView.findViewById(R.id.txtPasswordWrap);
        final AppCompatEditText txtPassword = promptView.findViewById(R.id.txtPassword);
        AppCompatCheckBox chkChangeSsidHidden = promptView.findViewById(R.id.chkChangeSsidHidden);
        AppCompatButton btnCancelChangeSsid = promptView.findViewById(R.id.btnCancelChangeSsid);
        AppCompatButton btnChangeSsid = promptView.findViewById(R.id.btnChangeSsid);

        txtSsid.setText(wifiApManager.getWifiApConfiguration().SSID.replace("\"", ""));
        txtPassword.setText(wifiApManager.getWifiApConfiguration().preSharedKey);

        // Inflate and set the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout
        builder.setView(promptView);

        final AlertDialog alertDialog = builder.create();
        alertDialog.show();

        chkChangeSsidHidden.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isHidden = ((AppCompatCheckBox) v).isChecked();
            }
        });

        btnCancelChangeSsid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.cancel();
            }
        });

        btnChangeSsid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (txtSsid.getText().toString().trim().isEmpty()) {
                    txtSsidWrap.setError(getString(R.string.field_is_empty));
                } else if (txtPassword.getText().toString().trim().isEmpty()) {
                    txtPasswordWrap.setError(getString(R.string.field_is_empty));
                } else if (txtPassword.getText().toString().trim().length() < 8) {
                    txtPasswordWrap.setError(getString(R.string.at_least_8_chars));
                } else {
                    if (wifiApManager.setWifiApEnabled(
                            mUtils.setWifiConfig(
                                    txtSsid.getText().toString().replace("\"", ""),
                                    txtPassword.getText().toString(),
                                    isHidden
                            ),
                            true
                    )) {
                        if (updateSsidOnServer())
                            mUtils.showSnackbar(refreshList, String.format("SSID changed to %s", txtSsid.getText().toString().trim()));
                    } else
                        mUtils.showSnackbar(refreshList, "An error occurred, please try again");

                    alertDialog.cancel();
                }
            }
        });
    }
}
