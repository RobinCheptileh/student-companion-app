package com.cognition.android.studentcompanionserver.utils;

import com.cognition.android.studentcompanionserver.pojo.ServerDevice;
import com.cognition.android.studentcompanionserver.pojo.Student;
import com.cognition.android.studentcompanionserver.pojo.Token;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.PATCH;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * API Interface
 */

public interface ApiInterface {
    @FormUrlEncoded
    @POST("api-token-auth/")
    Call<Token> getToken(
            @Field("username") String username,
            @Field("password") String password
    );

    @GET("students/{username}/")
    Call<Student> getStudentDetails(
            @Header("Authorization") String token,
            @Path("username") String username
    );

    @GET("server-devices/")
    Call<List<ServerDevice>> listServerDevices(
            @Header("Authorization") String token
    );

    @GET("server-devices/{id}/")
    Call<ServerDevice> getServerDevice(
            @Header("Authorization") String token,
            @Path("id") Integer id
    );

    @POST("server-devices/")
    Call<ServerDevice> registerServerDevice(
            @Header("Authorization") String token,
            @Body ServerDevice serverDevice
    );

    @FormUrlEncoded
    @PATCH("server-devices/{id}/")
    Call<ServerDevice> updateServerSsid(
            @Header("Authorization") String token,
            @Path("id") Integer id,
            @Field("ssid") String ssid
    );

    @FormUrlEncoded
    @PATCH("server-devices/{id}/")
    Call<ServerDevice> updateServerToken(
            @Header("Authorization") String token,
            @Path("id") Integer id,
            @Field("fa_token") String fa_token
    );
}
