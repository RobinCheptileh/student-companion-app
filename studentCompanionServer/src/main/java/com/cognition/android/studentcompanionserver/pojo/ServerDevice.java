package com.cognition.android.studentcompanionserver.pojo;


import com.cognition.android.studentcompanionserver.hotspotmanager.ClientScanResult;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ServerDevice {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("ssid")
    @Expose
    private String ssid;
    @SerializedName("mac_address")
    @Expose
    private String macAddress;
    @SerializedName("fa_token")
    @Expose
    private String faToken;
    private String authToken;

    /**
     * No args constructor for use in serialization
     */
    public ServerDevice() {
    }

    /**
     * Constructor without id
     *
     * @param ssid       String
     * @param macAddress String
     * @param faToken    String
     */
    public ServerDevice(String ssid, String macAddress, String faToken) {
        super();
        this.ssid = ssid;
        this.macAddress = macAddress;
        this.faToken = faToken;
    }

    public ServerDevice(String ssid, String macAddress, String faToken, String authToken) {
        super();
        this.ssid = ssid;
        this.macAddress = macAddress;
        this.faToken = faToken;
        this.authToken = authToken;
    }

    /**
     * Constructor
     *
     * @param id         Integer
     * @param ssid       String
     * @param macAddress String
     * @param faToken    String
     */
    public ServerDevice(Integer id, String ssid, String macAddress, String faToken) {
        super();
        this.id = id;
        this.ssid = ssid;
        this.macAddress = macAddress;
        this.faToken = faToken;
    }

    public ServerDevice(Integer id, String ssid, String macAddress, String faToken, String authToken) {
        super();
        this.id = id;
        this.ssid = ssid;
        this.macAddress = macAddress;
        this.faToken = faToken;
        this.authToken = authToken;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSsid() {
        return ssid;
    }

    public void setSsid(String ssid) {
        this.ssid = ssid;
    }

    public String getMacAddress() {
        return macAddress;
    }

    public void setMacAddress(String macAddress) {
        this.macAddress = macAddress;
    }

    public String getFaToken() {
        return faToken;
    }

    public void setFaToken(String faToken) {
        this.faToken = faToken;
    }

    public String getAuthToken() {
        return authToken;
    }

    public void setAuthToken(String authToken) {
        this.authToken = authToken;
    }

    public com.cognition.android.studentcompanionserver.models.ServerDevice getModel() {
        return new com.cognition.android.studentcompanionserver.models.ServerDevice(
                this.id, this.ssid, this.macAddress, this.faToken, this.authToken
        );
    }

    @Override
    public String toString() {
        return String.format("SSID: %s\tMAC: %s\tToken: %s", this.ssid, this.macAddress, this.faToken);
    }
}

